<?php
/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/
Version: 2.1
*/
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

//validator is builtin class in laravel
use Validator;

use App;
use Lang;

use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;
use App\Libraries\Slug;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 
use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;


class AdminDescriptionController extends Controller
{
	private $slug;

    public function __construct(Slug $slug)
    {
		$this->slug = $slug;
    }

    public function listingDescription(Request $request){
		$title = array('pageTitle' => Lang::get("labels.listingDescription"));		
		
		$result = array();
		$message = array();
			
		$description = DB::table('description')->paginate(20);
		
		$result['message'] = $message;
		$result['description'] = $description;
		
		return view("admin.listingDescription", $title)->with('result', $result);
	}

	public function addDescription(Request $request){
		$title = array('pageTitle' => Lang::get("labels.Service"));
		$customers = \DB::table('customers')->get();
		$result = array();
		$message = array();
		//$result['companies'] = DB::table('companies')->get();
		$result['message'] = $message;
		return view("admin.addDescription",['regstn'=>$customers], $title)->with('result', $result);
	}
	public function addNewDescription(Request $request){
		$title = array('pageTitle' => Lang::get("labels.Service"));
		
		$id = DB::table('description')->insertGetId([
			'regid' => $request->regid,
			'description'  =>  $request->description,
		]);
		$message = "Details has been added successfully!";
		return redirect()->back()->withErrors([$message]);
	}
	
	public function editDescription(Request $request){		
		$title = array('pageTitle' => Lang::get("labels.EditDetails"));
		$result = array();		
		$result['message'] = array();
		$description = DB::table('description')->where('id', $request->id)->get();
		$result['description'] = $description;
		return view("admin.editDescription",$title)->with('result', $result);
	}
	public function updateDescription(Request $request){
		$title = array('pageTitle' => Lang::get("labels.EditDetails"));
		
		
		$message = "Description has been updated successfully!";
		$companyUpdate = DB::table('description')->where('id', $request->id)->update([
			'description'	   =>   $request->description
		]);
		return redirect()->back()->withErrors([$message ]);
	}
	public function deleteDescription(Request $request){
		DB::table('description')->where('id', $request->id)->delete();
		return redirect()->back()->withErrors("Details Deleted");
	}

	
}                                                                                     
