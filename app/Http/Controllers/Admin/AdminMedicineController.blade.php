<?php
/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/
Version: 2.1
*/
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

//validator is builtin class in laravel
use Validator;

use App;
use Lang;

use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;
use App\Libraries\Slug;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 
use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;


class AdminMedicineController extends Controller
{
	private $slug;

    public function __construct(Slug $slug)
    {
		$this->slug = $slug;
    }

    public function listingMedicine(Request $request){
		$title = array('pageTitle' => Lang::get("labels.listingMedicine"));		
		
		$result = array();
		$message = array();
			
		$review = DB::table('review')->paginate(20);
		
		$result['message'] = $message;
		$result['review'] = $review;
		
		return view("admin.listingMedicine", $title)->with('result', $result);
	}

	public function addMedicine(Request $request){
		$title = array('pageTitle' => Lang::get("labels.Service"));
		$description = \DB::table('description')->get();
		$result = array();
		$message = array();
		//$result['companies'] = DB::table('companies')->get();
		$result['message'] = $message;
		return view("admin.addMedicine",['dsriptn'=>$description], $title)->with('result', $result);
	}
	public function addNewMedicine(Request $request){
		$title = array('pageTitle' => Lang::get("labels.Service"));
		
		$id = DB::table('review')->insertGetId([
			'desid' => $request->desid,
			'medicine'  =>  $request->medicine,
			'medicinedesc'  =>  $request->medicinedesc,
			'time'  =>  implode(',', (array) $request->get('time')),
		]);
		$message = "Details has been added successfully!";
		return redirect()->back()->withErrors([$message]);
	}
	
	public function editMedicine(Request $request){		
		$title = array('pageTitle' => Lang::get("labels.EditDetails"));
		$result = array();		
		$result['message'] = array();
		$review = DB::table('review')->where('id', $request->id)->get();
		$result['review'] = $review;
		return view("admin.editMedicine",$title)->with('result', $result);
	}
	public function updateMedicine(Request $request){
		$title = array('pageTitle' => Lang::get("labels.EditDetails"));
		
		
		$message = "Description has been updated successfully!";
		$companyUpdate = DB::table('review')->where('id', $request->id)->update([
			'desid' => $request->desid,
			'medicine'  =>  $request->medicine,
			'medicinedesc'  =>  $request->medicinedesc,
			'time'  =>  implode(',', (array) $request->get('time')),
		]);
		return redirect()->back()->withErrors([$message ]);
	}
	public function deleteMedicine(Request $request){
		DB::table('review')->where('id', $request->id)->delete();
		return redirect()->back()->withErrors("Details Deleted");
	}

	
}                                                                                     
