<?php
/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/
Version: 2.1
*/
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

//validator is builtin class in laravel
use Validator;

use App;
use Lang;

use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;
use App\Libraries\Slug;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 
use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;


class AdminTimeController extends Controller
{

	private $slug;

    public function __construct(Slug $slug)
    {
    	$this->slug = $slug;
	}
	//listingTaxClass
	public function listingTime(Request $request){
		$title = array('pageTitle' => Lang::get("labels.listingTime"));		
		
		$result = array();
		$message = array();
			
		$time = DB::table('time')->paginate(20);
		
		$result['message'] = $message;
		$result['time'] = $time;
		
		return view("admin.listingTime", $title)->with('result', $result);
	}

	//addTaxClass

	public function addTime(Request $request){
		$title = array('pageTitle' => Lang::get("labels.AddCompanies"));
		$result = array();
		$message = array();
		$result['message'] = $message;
		return view("admin.addTime", $title)->with('result', $result);
	}
	public function addNewTime(Request $request){
		$title = array('pageTitle' => Lang::get("labels.AddCompanies"));
		$newsDate = str_replace('/', '-', $request->datee);
		$newsDateFormate = date('Y-m-d H:i:s', strtotime($newsDate));
		
		$id = DB::table('time')->insertGetId([
				'time'   =>   $request->time,
				'datee'      =>   $newsDateFormate,
				
		]);
		$message = "Time has been added successfully!";
		return redirect()->back()->withErrors([$message]);
	}
	//editTaxClass
	public function editTime(Request $request){		
		$title = array('pageTitle' => Lang::get("labels.EditEvents"));
		$result = array();		
		$result['message'] = array();
		
		$time = DB::table('time')->where('id', $request->id)->get();
		$result['time'] = $time;
		return view("admin.editTime",$title)->with('result', $result);
	}
	//updateTaxClass
	public function updateTime(Request $request){
			$title = array('pageTitle' => Lang::get("labels.EditEvents"));
			$newsDate = str_replace('/', '-', $request->datee);
			$newsDateFormate = date('Y-m-d H:i:s', strtotime($newsDate));

			

			$message = "Updated successfully!";
			$companyUpdate = DB::table('time')->where('id', $request->id)->update([
				'time'   =>   $request->time,
				'datee'      =>   $newsDateFormate,
			]);
			return redirect()->back()->withErrors([$message ]);
	}
	//deleteCountry
	public function deleteTime(Request $request){
		DB::table('time')->where('id', $request->id)->delete();
		return redirect()->back()->withErrors("Deleted");
	}
	
}                                                                                     
