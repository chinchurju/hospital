<?php

namespace App\Http\Controllers\Admin\subscribers;
use App\Http\Controllers\Controller;


//validator is builtin class in laravel
use Validator;
use App;
use Lang;
use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 
use Illuminate\Http\Request;
class AdminSubscribersController extends Controller
{
	public function listing(Request $request){
		$title = array('pageTitle' => "Subscribers");
		//$language_id            				=   $request->language_id;
		$language_id            				=   '1';			
		$results								= array();
		
		$data = DB::table('subscribers') 
			->select('*');
		
		$subscribers = $data->orderBy('subscriber_id', 'DESC')->paginate(10);	
		
		$results['subscribers'] = $subscribers;
		
	
		$currentTime =  array('currentTime'=>time());
		//return view("admin.subscribers.list",$title)->with('results', $results);
		return view('admin.subscribers.list', [
            'subscribers' => $subscribers
        ]);
	}
}
