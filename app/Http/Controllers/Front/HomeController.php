<?php
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use DB;
use Validator;
use Auth;
use PDF;
use App\Shop\Customers\Requests\RegisterCustomerRequest;
use App\Shop\Customers\Requests\AppointmentCustomerRequest;
use Illuminate\Http\Request;
use App\Shop\Customers\Customer;


class HomeController extends Controller
{
  
    public function index()
    {
        $view = 'front.index';
        return view($view);
    }

    public function login()
    {
        $view = 'front.login';
        return view($view);
    }
	
	public function loginn()
    {
        $view = 'front.login';
        return view($view);
    }
	
	public function client()
	{
		// echo "<pre>";
		// print_r(auth()->user()->toArray());
		// exit;
		$view = 'front.client';
		return view($view);
	}
	public function appointments()
	{
		//echo "dddddd";exit;
		// echo "<pre>";
		// print_r(auth()->user()->toArray());
		// exit;
		$appointments = DB::table('appointments')->where('regid',auth()->user()->id)->orderBy('id','DESC')->get();
		//print_r($appointments);exit;
		$view = 'front.booking';
		return view($view,['appointments' => $appointments]);
	}
	public function appprofile()
	{
		$view = 'front.app-profile';
		return view($view);
	}
	public function medicine()
	{

	    $id = auth()->user()->id;
		$users = DB::table('description')
            ->join('review', 'description.id', '=', 'review.desid')->where('regid', '=', $id)->get();
          
		$view = 'front.medicine';
		return view($view,['users' => $users]);
	}
	public function deceased()
	{

		$id = auth()->user()->id;
		$users = \DB::table('description')->where('regid', '=', $id)->get();
		$view = 'front.deceased';
		return view($view,['users' => $users]);
	}
	
	public function registration()
    {
        $view = 'front.registration';
        return view($view);
    }

    public function refreshCaptcha()
    {
    	return response()->json(['captcha'=> captcha_img()]);
    }
	
	public function register(RegisterCustomerRequest $request)
	{

		$v = Validator::make($request->all(), [
		    'name' => 'required|min:3|max:90',
		    'password' => 'required|min:5|confirmed',
		    'password_confirmation' => 'required|min:5',
		    'captcha' => 'required|captcha'
	    ]);

	    if ($v->fails()) {
	    	$message = "name and password is required";
		   	return redirect()->back()->withErrors([$message]);
		}
		 $details = $request->only('email', 'password');
		//print_r($details);exit;
        $details['status'] = 1;
		$user = Customer::login($details);
        if ($user) {
			$user->delete();
		}
		$tokenC = Customer::where('datee',$request->datee)->get();
		
		$id = DB::table('customers')->insertGetId([
            'name'      =>  $request->name,
			'address'   =>  $request->address,
			'place'     =>  $request->place,
			'gender'    =>  $request->gender,
			'dob'       =>  $request->dob,
			'phone'     =>  $request->phone,
			'tokenno'  =>   ((int)$tokenC->count())+1,
			//'datee'     =>  $request->datee,
			'datee'     =>  $request->datee,
			'email'     =>  $request->email,
			'password'  =>  md5($request->password),
			'password_confirmation' =>  ($request->get('same:password')),
			'timee'     =>  $request->timee,
		]);

		DB::table('appointments')->insert([
			'name'     =>  $request->name,
			'regid'    =>  $id,
			'tokenno'  =>  ((int)$tokenC->count())+1,
			'datee'    =>  $request->datee,
			'address'  =>  $request->address,
			'place'    =>  $request->place,
			'gender'   =>  $request->gender,
			'dob'      =>  $request->dob,
			'phone'    =>  $request->phone,
			'timee'    =>  $request->timee,
			'status'   =>  'Active',
		]);
		
		$message = "Appointment has been submitted successfully!";
        return redirect()->back()->withErrors([$message]);
	}


	public function generatePDF(Request $request)
    {
		if($request->input('id')){
			$customers =(array)\DB::table('appointments')->where('id', $request->input('id'))->first();
		}else{
			$customers = auth()->user();
		}
		//print_r($customers);exit;
        $pdf = PDF::loadView('front.myPDF', $customers);
  
        return $pdf->download('detaiils.pdf');


    }


    public function generatemedicinePDF(Request $request)
	{
		$users = DB::table('description')
            ->join('review', 'description.id', '=', 'review.desid')
            ->get();
        $pdf = PDF::loadView('front.myPDFmedicine', ['users' => $users]);
        return $pdf->download('details.pdf');
	}

	 public function generatedesPDF(Request $request)
	{

		$id = auth()->user()->id;
		$users = \DB::table('description')->where('regid', '=', $id)->get();
        $pdf = PDF::loadView('front.myPDFdes', ['users' => $users]);
        return $pdf->download('details.pdf');
	}
	public function newAppointments()
    {
        $view = 'front.newbooking';
        return view($view);
    }
	public function saveNewAppointments(AppointmentCustomerRequest $request)
	{

		$tokenC = Customer::where('datee',$request->datee)->get();
		
		$id = DB::table('customers')->update([
            
			'tokenno'  =>   ((int)$tokenC->count())+1,
			//'datee'     =>  $request->datee,
			'datee'     =>  $request->datee,
			'timee'     =>  $request->timee,
		]);

		DB::table('appointments')->insert([
			'name'     =>  auth()->user()->name,
			'regid'    =>  auth()->user()->id,
			'tokenno'  =>  ((int)$tokenC->count())+1,
			'datee'    =>  $request->datee,
			'timee'    =>  $request->timee,
			'status'   =>  'Active',
		]);
		
		
		
		$message = "Appointment has been submitted successfully!";
        return redirect()->back()->withErrors([$message]);
	}
	

}
