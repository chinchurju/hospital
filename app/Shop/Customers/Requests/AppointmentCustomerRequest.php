<?php

namespace App\Shop\Customers\Requests;
use App\Rules\Customrule;
use App\Shop\Base\BaseFormRequest;

class AppointmentCustomerRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'datee' => [new Customrule()]
        ];
    }
}
