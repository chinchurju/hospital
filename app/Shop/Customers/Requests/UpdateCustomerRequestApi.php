<?php

namespace App\Shop\Customers\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class UpdateCustomerRequestApi extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $requests)
    {
		if($requests->input('type') == "details"){
			return [
				'name' => ['required'],
				//'email' => ['required', 'email', Rule::unique('customers')->ignore(auth()->user()->id)],
				'profile' => ['file', 'image:png,jpeg,jpg,gif']
			];
		}
		if($requests->input('type') == "password"){
			return [
				'current-password' => ['required'],
				'password' => ['required'],
				//'c-password' => ['required'],
				
			];
		}
    }
}
