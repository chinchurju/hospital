<?php

namespace App\Shop\Customers\Transformations;

use App\Shop\Customers\Customer;

trait CustomerTransformable
{
    protected function transformCustomer(Customer $customer)
    {
        $prop = new Customer;
        $prop->id = (int) $customer->id;
        $prop->name = $customer->name;
        $prop->profile_pic = $customer->profile_pic;
		$prop->email = $customer->email;
		$prop->phone = $customer->phone;
        $prop->status = (int) $customer->status;

        return $prop;
    }
}
