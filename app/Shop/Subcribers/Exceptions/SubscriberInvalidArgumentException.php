<?php

namespace App\Shop\Subscribers\Exceptions;

use Doctrine\Instantiator\Exception\InvalidArgumentException;

class SubscriberInvalidArgumentException extends InvalidArgumentException
{
}
