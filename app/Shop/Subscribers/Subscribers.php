<?php

namespace App\Shop\Subscribers;

use Illuminate\Database\Eloquent\Model;
use DB;
class Subscribers extends Model
{
    public function createSubscribers($data){
		$data['created_date'] = date("Y-m-d H:i:s");
		$dd =  DB::table('subscribers')->insertGetId(
			$data
		);
	}
	public function getSubscribers($data){
		return DB::table('subscribers')->where('email_address',$data)->get();
	}
}
