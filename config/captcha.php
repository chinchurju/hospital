<?php
    return [
        'default'   => [
			'length'    => 5,
			'width'     => 200,
			'height'    => 60,
			'quality'   => 100,
			//'math'      => true, //Enable Math Captcha
		],
    ];