@extends('layouts.admin.app')
@section('content')
<section class="content">
    <!-- Default box -->
  <div class="box">
    <div class="box-body">
      <h2>Add Description</h2>
         <!-- /.box-header -->
      <div class="box-body">
		    <div class="row">
          <div class="col-md-12">
            <div class="box">
           <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <!-- /.box-header -->
                    <!-- form start -->                        
                    <div class="box-body">
                      @if( count($errors) > 0)
                      @foreach($errors->all() as $error)
                      <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        {{ $error }}
                      </div>
                      @endforeach
                      @endif
                       {!! Form::open(array('url' =>'admin/addNewDescription', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                      
							        <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Registration Id</label>
                        <div class="col-sm-10 col-md-4">
          							<select class="form-control" name="regid" id="regid">
          								<option value="">Select </option>
          								  @if($regstn->count() > 0)
          								  @foreach($regstn as $row)
          								<option value="{{$row->id}}">{{$row->id}}</option>
          						          @endforeach	
          								  @endif
          							</select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Description') }}  </label>
                        <div class="col-sm-10 col-md-8">
                          <textarea id="editor" name="description" class="form-control" rows="10" cols="80"></textarea> <br>
                        </div>
                      </div>

                    
                     <!-- /.box-body -->
        							<div class="box-footer text-center">
        								<button type="submit" class="btn btn-primary">{{ trans('labels.SubmitNews') }}</button>
        								<a href="{{ URL::to('admin/listingDescription')}}" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
        							</div>
                       <!-- /.box-footer -->
                       {!! Form::close() !!}
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
          </div>
           <!-- /.col --> 
        </div>
        <!-- /.row --> 
      </div>
        <!-- Main row --> 
    </div> 
	</div>
    <!-- /.row --> 
</section>
  <!-- /.content --> 
<script src="{!! asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>
<script type="text/javascript">
		$(function () {
			//bootstrap WYSIHTML5 - text editor
			//
			$("textarea").summernote({height: "400px",});
			//$("textarea").wysihtml5();
			
    });
</script>
@endsection 