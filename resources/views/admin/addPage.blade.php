@extends('layouts.admin.app')
@section('content')
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Add Page</h2>
          <!-- /.box-header -->
          <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
         
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
                        <!-- /.box-header -->
                        <!-- form start -->                        
                         <div class="box-body">
                          @if( count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-success" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Error:</span>
                                    {{ $error }}
                                </div>
                             @endforeach
                          @endif
                        
                            {!! Form::open(array('url' =>'admin/addNewPage', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                            
                                 <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Template</label>
                                  <div class="col-sm-10 col-md-4">
                                      <select class="form-control" name="type">
                                          <option value="1">Default</option>
                                          <option value="0">Tribute</option>
                                      </select>
                                  </div>
                                </div>

                              <div class="form-group">
                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.PageSlug') }}</label>
                                 <div class="col-sm-10 col-md-4">
                                 {!! Form::text('slug',  '', array('class'=>'form-control field-validate', 'id'=>'slug')) !!}
                                </div>
                              </div>
                              <div class="form-group">
                               <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.PageName') }}</label>
                                <div class="col-sm-10 col-md-4">
                                 {!! Form::text('name',  '', array('class'=>'form-control field-validate', 'id'=>'name')) !!}
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Description') }}  </label>
                                <div class="col-sm-10 col-md-8">
                                  <textarea id="editor" name="description" class="form-control" rows="10" cols="80"></textarea> <br>
                                </div>
                              </div>
                               
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Status') }}</label>
                                  <div class="col-sm-10 col-md-4">
                                      <select class="form-control" name="status">
                                          <option value="1">{{ trans('labels.Active') }}</option>
                                          <option value="0">{{ trans('labels.InActive') }}</option>
                                      </select>
                                   
                                  </div>
                                </div>
                                
                             
              
                              <!-- /.box-body -->
              <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary">{{ trans('labels.AddPage') }}</button>
                <a href="{{ URL::to('admin/listingCompanies')}}" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
              </div>
                              <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
              </div>
            </div>
            
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
    </div>
    <!-- Main row --> 
    </div> 
  
  </div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 

<script src="{!! asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>


<script type="text/javascript">
    $(function () {
      
      //bootstrap WYSIHTML5 - text editor
      //
      $("textarea").summernote({height: "400px",});
      //$("textarea").wysihtml5();
      
    });
</script>
@endsection 