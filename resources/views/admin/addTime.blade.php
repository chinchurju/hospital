@extends('layouts.admin.app')
@section('content')
<section class="content">
    <!-- Default box -->
  <div class="box">
    <div class="box-body">
      <h2>Add Time</h2>
         <!-- /.box-header -->
      <div class="box-body">
		    <div class="row">
          <div class="col-md-12">
            <div class="box">
           <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <!-- /.box-header -->
                    <!-- form start -->                        
                    <div class="box-body">
                      @if( count($errors) > 0)
                      @foreach($errors->all() as $error)
                      <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        {{ $error }}
                      </div>
                      @endforeach
                      @endif
                       {!! Form::open(array('url' =>'admin/addNewTime', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
            

                    <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Time</label>
                        <div class="col-sm-10 col-md-4">
                         <input type="text" name="time" class="form-control field-validate">
                        </div>
                    </div>
					
					
                    <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Date</label>
                        <div class="col-sm-10 col-md-4">
                            <input readonly class="form-control datepicker field-validate" type="text" name="datee" value="">
                        </div>
                    </div>

                    
                     <!-- /.box-body -->
        							<div class="box-footer text-center">
        								<button type="submit" class="btn btn-primary">{{ trans('labels.SubmitNews') }}</button>
        								<a href="{{ URL::to('admin/listingTime')}}" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
        							</div>
                       <!-- /.box-footer -->
                       {!! Form::close() !!}
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
          </div>
           <!-- /.col --> 
        </div>
        <!-- /.row --> 
      </div>
        <!-- Main row --> 
    </div> 
	</div>
    <!-- /.row --> 
</section>
  <!-- /.content --> 
<script src="{!! asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>
<script type="text/javascript">
		$(function () {
			//bootstrap WYSIHTML5 - text editor
			//
			$("textarea").summernote({height: "400px",});
			//$("textarea").wysihtml5();
			
    });
</script>
@endsection 