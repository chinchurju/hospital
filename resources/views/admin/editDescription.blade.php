@extends('layouts.admin.app')
@section('content')
<section class="content">
    <div class="box">
        <div class="box-body">
            <h2>Edit Details</h2>
           	<div class="box-body">
            <div class="row">
                <div class="col-xs-12">
              		<div class="box box-info">
                       <br>                       
                        @if (count($errors) > 0)
                              @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{$errors->first()}}
                                </div>
                              @endif
                          @endif
                       
						<div class="panel with-nav-tabs  panel-default">
							
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="English">
										<div class="box-body">
											{!! Form::open(array('url' =>'admin/updateDescription', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
											  
						                    {!! Form::hidden('id',  $result['description'][0]->id , array('class'=>'form-control', 'id'=>'id')) !!}

												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Register Id</label>
												  <div class="col-sm-10 col-md-4">
													{!! Form::text('regid', $result['description'][0]->regid, array('class'=>'form-control','id'=>'regid','readonly')) !!}
												  </div>
												</div>

												<div class="form-group">
					                                <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Description') }}  </label>
					                                <div class="col-sm-10 col-md-8">
					                                  <textarea id="editor" name="description" class="form-control" rows="10" cols="80">{!! $result['description'][0]->description!!}</textarea> <br>
					                                </div>
				                             	</div>

											    <div class="box-footer text-center">
													<button type="submit" class="btn btn-primary">{{ trans('labels.Update') }}</button>
													<a href="{{ URL::to('admin/listingDescription')}}" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
											    </div>
											{!! Form::close() !!}
										</div>
									</div>
                  				</div>
              				</div>
            			</div>
         			</div>
        		</div>
   			</div>
</section>

<script src="{!! asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>

<script type="text/javascript">
		$(function () {
			
			$("textarea").summernote({height: "400px",});
			
    });
</script>
@endsection 