@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Board of Directors</h2>
            <div class="row">
              <div class="col-xs-12">
           <div class="btn-group pull-right" >
                        <a class="btn btn-sm btn-primary" href="{{ route('admin.addDirectors') }}"><i class="fa fa-plus"></i> Add New</a>
                 </div>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Position</th>
                      <th>Level</th>
                      <th>Image</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(count($result['boardofdirector'])>0)
                    @foreach ($result['boardofdirector'] as $key=>$boardofdirector)
                        <tr>
                            <td>{{ $boardofdirector->id }}</td>
                            <td>{{ $boardofdirector->name }}</td>
                            <td>{{ $boardofdirector->position }}</td>
                            <td>{{ $boardofdirector->level }}</td>
                            <td><img src="{{asset('').'/'.$boardofdirector->image}}" alt="" width=" 100px"></td>
                            <td>{{ $boardofdirector->status }}</td>
                            <td><a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editDirectors/{{ $boardofdirector->id }}" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                            
                            <a data-toggle="tooltip" data-placement="bottom" title="Delete" id="deleteboardofdirectorId" boardofdirectorid ="{{ $boardofdirector->id }}" class="badge bg-red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </tr>
                    @endforeach
                    @else
                       <tr>
                            <td colspan="5">NoRecordFound</td>
                       </tr>
                    @endif
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                  {{$result['boardofdirector']->links()}}
                </div>
              </div>
            </div>
          </div>
     </section>  

    <!-- deleteBannerModal -->
  <div class="modal fade" id="deleteboardofdirectorModal" tabindex="-1" role="dialog" aria-labelledby="deleteboardofdirectorModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="deleteboardofdirectorModalLabel">Delete Director</h4>
      </div>
      {!! Form::open(array('url' =>'admin/deleteDirectors', 'name'=>'deleteDirectors', 'id'=>'deleteDirectors', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
          {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
          {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'id')) !!}
      <div class="modal-body">            
        <p>Are you sure you want to delete this Director?</p>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary" id="deleteBanner">Delete</button>
      </div>
      {!! Form::close() !!}
    </div>
    </div>
  </div>
       
@endsection 
@section('js')
<script>
$(document).on('click', '#deleteboardofdirectorId', function(){
var id = $(this).attr('boardofdirectorid');
$('#id').val(id);
$("#deleteboardofdirectorModal").modal('show');
});
</script> 
@endsection