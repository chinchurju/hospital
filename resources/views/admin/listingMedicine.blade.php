@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Descriptions</h2>
            <div class="row">
              <div class="col-xs-12">
                <div class="btn-group pull-right" >
                  <a class="btn btn-sm btn-primary" href="{{ route('admin.addMedicine') }}"><i class="fa fa-plus"></i> Add New</a>
                </div>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Description Id</th>
                      <th>Medicines</th>
                      <th>Medicine Description</th>
                      <th>Time</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(count($result['review'])>0)
                    @foreach ($result['review'] as $key=>$review)
                        <tr>
                            <td>{{ $review->id }}</td>
                            <td>{{ $review->desid }}</td>
                            <td>{!! substr($review->medicine,0,200 )!!}</td>
                            <td>{!! substr($review->medicinedesc,0,200 )!!}</td>
                            <td>{{ $review->time }}</td>
                        </tr>
                    @endforeach
                    @else
                       <tr>
                          <td colspan="5">NoRecordFound</td>
                       </tr>
                    @endif
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                  {{$result['review']->links()}}
                </div>
              </div>
            </div>
          </div>
     </section>  



@endsection