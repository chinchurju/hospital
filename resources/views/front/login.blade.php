@extends('layouts.front.apphome')
@section('content')
 <div class="fullsize-container">
                <div class="fullsize-slider">
                    <ul>
               
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="http://placehold.it/200x200" data-delay="7000" data-saveperformance="off" data-title="Slide" style="color: white;">
                            <img src="{!! asset('resources/assets/front') !!}/images/banner1.png" alt="fullslide6" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                           
                        </li>
                    </ul>
                </div>
            </div> 
  <div class="container space-sm">
                    <div class="col-md-12">
                        <div class="book-box row">
                            <div class="book-form">
                                <h4 class="hr-after" style="text-align: center;">Login</h4>
							 {!! Form::open(array('url' =>'loginn', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data', 'id' => 'sub')) !!}
                                <div class="row">
                                    <p class="col-md-6 col-sm-6" style="margin-left: 285px;">
                                        <input class="form-control" name="name" type="text" placeholder="Name" required="">
                                    </p>
                                    
                                </div>
								
								 <div class="row">
                                    
                                    <p class="col-md-6 col-sm-6" style="margin-left: 285px;">
                                        <input class="form-control" type="text" name="password" placeholder="password" required="">
                                    </p>
                                </div>
								<div class="row" style="margin-left: 500px;">
									<button id="reply_form_submit" type="submit" class="reply_submit_btn trans_300" value="Submit">
										Login
									</button>
								</div>

                                
                                 <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
             <div class="captcha">
               <span>{!! captcha_img() !!}</span>
               <button type="button" class="btn btn-success"><i class="fa fa-refresh" id="refresh"></i></button>
               </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
             <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha"></div>
          </div>
								
								
							</div>
                        </div>
                     
                    </div>
                
                </div>
@endsection