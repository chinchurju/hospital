 
<!DOCTYPE html>
<html>
<head>
	<title>Hi</title>
</head>
<body>
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
          <!-- /.box-header -->
           <h3>Details</h3>
         		<table class="table table-bordered table-striped" style="border: 1px solid #dadadae0; margin-top: 7px;">
                  <thead>
                    <tr>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Name</th>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Address</th>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Email</th>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Phone</th>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Gender</th>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Place</th>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Date</th>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Time</th>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Token.no</th>
                    </tr>
                  </thead>
                    <tbody>
               
                        <tr>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{{auth()->user()->name}}</td>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{{auth()->user()->address}}</td>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{{auth()->user()->email}}</td>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{{auth()->user()->phone}}</td>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{{auth()->user()->gender}}</td>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{{auth()->user()->place}}</td>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{{$datee}}</td>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{{$timee}}</td>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{{$tokenno}}</td>
                        </tr>
                   
                  </tbody>
                </table>
            




        </div>
        <!-- /.box --> 
    </div>
   
  </section>
 </body>
</html>
