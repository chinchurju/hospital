<!DOCTYPE html>
<html>
<head>
  <title>Hi</title>
</head>
<body>
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
          <!-- /.box-header -->
           <h3>Details</h3>
            <table class="table table-bordered table-striped" style="border: 1px solid #dadadae0; margin-top: 7px;">
                  <thead>
                    <tr>
                       <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Name</th>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Medicine Name</th>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Medicine Description</th>
                      <th style="border-bottom: 1px solid #d0cdcd7d;padding: 11px;border-right:1px solid #d0cdcd7d;">Time</th>
                     
                    </tr>
                  </thead>
                    <tbody>
               
                          <tr>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{{auth()->user()->name}}</td>
                             @foreach ($users as $user) 
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{!! $user->medicine !!}</td>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{!! $user->medicinedesc !!}</td>
                            <td style="border-right:1px solid #d0cdcd7d;text-align: center;">{!! $user->time !!}</td>
                         
                             @endforeach
                        </tr>

                  
                  </tbody>
                </table>
            




        </div>
        <!-- /.box --> 
    </div>
   
  </section>
 </body>
</html>
                                      
                                    