<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 0.1.0
    </div>
    <strong><a href="{{config('app.url')}}">{{config('app.name')}}</a></strong> 
</footer>