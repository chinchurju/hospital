<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Codenpixel">
    <title>ModernClinic</title>
    <link href="{!! asset('resources/assets/front') !!}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{!! asset('resources/assets/front') !!}/css/fonts.css" rel="stylesheet">
    <link href="{!! asset('resources/assets/front') !!}/css/animsition.css" rel="stylesheet">
    <link href="{!! asset('resources/assets/front') !!}/MegaNavbar/assets/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet">
    <link href="{!! asset('resources/assets/front') !!}/css/owl.carousel.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{!! asset('resources/assets/front') !!}/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{!! asset('resources/assets/front') !!}/MegaNavbar/assets/css/MegaNavbar.min.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('resources/assets/front') !!}/MegaNavbar/assets/css/skins/navbar-default.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('resources/assets/front') !!}/MegaNavbar/assets/css/animation/animation.css">
    <link href="{!! asset('resources/assets/front') !!}/css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{!! asset('resources/assets/front') !!}/css/daterangepicker/daterangepicker-bs3.css">
  	<!-- bootstrap datepicker -->
  	<link rel="stylesheet" type="text/css" href="{!! asset('resources/assets/front') !!}/css/datepicker/datepicker3.css">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    </head>

	<body>
		@include('layouts.front.header')

		@yield('content')

		@include('layouts.front.footer')

<script src="{!! asset('resources/assets/front') !!}/js/jquery.min.js"></script>
    <script src="{!! asset('resources/assets/front') !!}/js/bootstrap.min.js"></script>
    <script src="{!! asset('resources/assets/front') !!}/js/owl.carousel.min.js"></script>
    <script src="{!! asset('resources/assets/front') !!}/js/plugins.js"></script>
    <script type="text/javascript" src="{!! asset('resources/assets/front') !!}/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="{!! asset('resources/assets/front') !!}/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script src="{!! asset('resources/assets/front') !!}/js/custom.js"></script>
    <script src="{!! asset('resources/assets/front') !!}/js/ie10-viewport-bug-workaround.js"></script>
	<script type="text/javascript" src="{!! asset('resources/assets/front') !!}/js/datepicker/bootstrap-datepicker.js"></script> 

		<script>

$(function () {
$('.datepicker').datepicker({
		  autoclose: true
		});
	
});

</script>
<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>
</body>

</html>
