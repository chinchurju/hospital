    <div class="animsition">
        <div class="wrapper">
        <header id="header">
                <!-- Top Header Section Start -->
                <div class="top-bar bg-light hdden-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 list-inline list-unstyled no-margin hidden-xs">
                                <p class="no-margin">Have any questions? <strong>+080 124 880</strong> or mail@modernclinic.com</p>
                            </div>
                            <div class="btn-group pull-right col-sm-6">
                                <button class="dropdown-toggle language" type="button" data-toggle="dropdown" aria-expanded="false">
                                    <img src="{!! asset('resources/assets/front') !!}/images/us.png" width="16" height="11" alt="EN Language">English <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu no-border-radius no-shadow">
                                    <li>
                                        <a href="#">
                                            <img src="{!! asset('resources/assets/front') !!}/images/us.png" width="16" height="11" alt="EN Language">[US] English
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="{!! asset('resources/assets/front') !!}/images/de.png" width="16" height="11" alt="DE Language">[DE] German
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="{!! asset('resources/assets/front') !!}/images/fr.png" width="16" height="11" alt="FR Language">[FR] French
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="{!! asset('resources/assets/front') !!}/images/ru.png" width="16" height="11" alt="RU Language">[RU] Russian
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.top bar -->
                <!-- begin Logo and info -->
                <div class="container middle-bar hidden-xs">
                    <div class="row">
                        <a href="{{route('home')}}" class="logo col-sm-3">
                <img src="http://localhost/party/images/logo-dark.png" class="img-responsive" alt="" />
                        </a>
                        <div class="col-sm-8 col-sm-offset-1 contact-info">
                              <p><a href="{{route('registration')}}" class="btn btn-primary" role="button">Book Appointment</a> <a href="{{route('login')}}" class="btn btn-primary" role="button">User Login </a>
                                <a href="http://localhost/modernclinic/admin/login" class="btn btn-primary" role="button">Admin Login</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.middle -->
                <!-- begin MegaNavbar-->
                <div class="nav-wrap-holder">
                    <div class="container" id="nav_wrapper">
                        <nav class="navbar navbar-static-top navbar-default no-border-radius xs-height100" id="main_navbar" role="navigation">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#MegaNavbarID">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                                    </button>
                                    <a href="{{route('registration')}}" class="navbar-brand logo col-sm-3 visible-xs-block">
                                        
                                    </a>
                                </div>
                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="MegaNavbarID">
                                    <!-- regular link -->
                                    <ul class="nav navbar-nav navbar-left">
                                        <li><a href="{{route('home')}}"><i class="fa fa-home"></i> <span class="hidden-sm">Home</span></a>
                                        </li>
                                        <li class="divider"></li>
                                        <li class="dropdown-full no-shadow no-border-radius">
                                            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><span class="icon-i-family-practice" aria-hidden="true"></span>&nbsp;<span class="hidden-sm hidden-md reverse">Appoinment</span></a>
                                          
                                        </li>
                                        <li class="divider"></li>
                                        <!-- dropdown active -->
                                        <li class="dropdown-full no-border-radius no-shadow">
                                            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-money"></i>&nbsp;<span class="hidden-sm hidden-md reverse">User Login</span></a>
                                         
                                        </li>
                                        <!-- divider -->
                                        <li class="divider"></li>
                                        <!-- dropdown short. Forced show on small devices on hover  -->
                                     
                                        <!-- divider -->
                                        <li class="divider"></li>
                                    </ul>
                                    <ul class="nav navbar-nav navbar-right">
                                        <li>
                                            <!-- search form -->
                                            <form class="navbar-form-expanded navbar-form navbar-left visible-lg-block visible-md-block visible-xs-block" role="search">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" data-width="80px" data-width-expanded="170px" placeholder="Search" name="query">
                                                    <span class="input-group-btn"><button class="btn btn-default" type="submit"><i class="fa fa-search"></i>&nbsp;</button></span>
                                                </div>
                                            </form>
                                        </li>
                                        <li class="dropdown-grid visible-sm-block">
                                            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-search"></i>&nbsp;</a>
                                            <div class="dropdown-grid-wrapper" role="menu">
                                                <ul class="dropdown-menu col-sm-6">
                                                    <li>
                                                        <form class="no-margin">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control">
                                                                <span class="input-group-btn"><button class="btn btn-default" type="submit">&nbsp;<i class="fa fa-search"></i></button></span>
                                                            </div>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="divider"></li>
                                       
                                        <li class="divider"></li>
                                        <!-- dropdown grid -->
                                        <li class="dropdown-full">
                                            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-envelope"></i>&nbsp;<span class="hidden-sm hidden-md hidden-lg reverse">Account</span><span class="caret"></span></a>
                                            <ul class="dropdown-menu no-shadow no-border-radius space-xs">
                                                <li class="col-sm-4">
                                                    <address>
                                             <br>
                                             <strong>Brand, Inc.</strong><br>
                                             123 Folsom Ave, Suite 600<br>
                                             San Francisco, CA 94107<br>
                                             <abbr title="Phone">Phone:</abbr> (123) 456-7890<br>
                                             <abbr title="Mobile Phone">Mobile:</abbr> (098) 765-4321
                                          </address>
                                                    <address>
                                             <strong>Full Name</strong><br>
                                             <a href="mailto:#">first.last@example.com</a>
                                          </address>
                                                    <div class="open-hours">
                                                        <p>Monday - Friday <span>8.00 - 17.00</span>
                                                        </p>
                                                        <p>Saturday <span>9.30 - 17.30</span>
                                                        </p>
                                                        <p>Sunday <span>9.30 - 15.00</span>
                                                        </p>
                                                    </div>
                                                </li>
                                                <li class="col-sm-7 col-sm-offset-1">
                                                    <div id="message"></div>
                                                    <form method="post" action="contact.php" name="contactform" id="contactform">
                                                        <fieldset>
                                                            <input class="form-control" name="name" type="text" id="name" size="30" value="" placeholder="Name" />
                                                            <br />
                                                            <input class="form-control" name="email" type="text" id="email" size="30" value="" placeholder="E-mail" />
                                                            <br />
                                                            <input class="form-control" name="phone" type="text" id="phone" size="30" value="" placeholder="Phone" />
                                                            <br />
                                                            <select class="form-control" name="subject" id="subject">
                                                                <option value="0" selected="selected">Select your reason</option>
                                                                <option value="1">Appointment</option>
                                                                <option value="2">Support</option>
                                                                <option value="3">Emergency</option>
                                                            </select>
                                                            <br />
                                                            <textarea class="form-control" name="comments" cols="40" rows="3" id="comments" style="width: 100%;" placeholder="Your comments"></textarea>
                                                            <p><span class="required">*</span> Are you human?</p>
                                                            <label for="verify" accesskey="V">&nbsp;&nbsp;&nbsp;3 + 1 =</label>
                                                            <input name="verify" type="text" id="verify" size="4" value="" style="width: 30px; border:1px solid #eee;" />
                                                            <br />
                                                            <br />
                                                            <input type="submit" class="btn btn-primary btn-fill" id="submit" value="Submit" />
                                                        </fieldset>
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- /.div nav wrap holder -->
            </header>