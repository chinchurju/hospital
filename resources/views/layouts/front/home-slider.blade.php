<div class="fullsize-container">
               <div class="fullsize-slider">
                  <ul>
                     <li data-transition="fade" data-slotamount="1" data-thumb="http://localhost/modernclinic/resources/assets/front/images/slide-24.jpg" data-delay="7500" data-saveperformance="off" data-title="Our Workplace">
                        <!-- MAIN IMAGE -->
                        <img src="http://localhost/modernclinic/resources/assets/front/images/slide-24.jpg" data-bgfit="cover" data-bgrepeat="no-repeat" alt="">
                        <!-- LAYERS -->
                        <div class="tp-caption theme-caption rs-parallaxlevel-4 transform " data-x="20" data-y="center" data-speed="700" data-voffset="-75" data-start="600" data-easing="Power3.easeInOut">Be seen.
                           <br>Be cared for.
                           <br>Be on your way.
                        </div>
                        <div class="tp-caption sfl rs-parallaxlevel-4" data-x="20" data-y="center" data-voffset="15" data-speed="1000" data-start="2000" data-easing=""><a href="#service" class="btn-round tp-simpleresponsive button blue">SCROLL DOWN</a>
                        </div>
                     </li>
                     <!-- SLIDE  -->
                     <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="http://localhost/modernclinic/resources/assets/front/images/slide-23.jpg" data-delay="7000" data-saveperformance="off" data-title="Slide">
                        <!-- MAIN IMAGE -->
                        <img src="http://localhost/modernclinic/resources/assets/front/images/slide-23.jpg" alt="fullslide6" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                        <div class="caption theme-caption rs-parallaxlevel-4 transform " data-x="20" data-y="center" data-speed="700" data-voffset="-95" data-start="600" data-easing="Power3.easeInOut">Be seen.
                           <br>Be cared for.
                           <br>Be on your way.
                        </div>
                        <div class="caption mediumlarge_light_dark rs-parallaxlevel-4" data-x="20" data-y="center" data-speed="800" data-voffset="-20" data-start="1500" data-easing="Power3.easeInOut">CREATIVITY</div>
                        <div class="caption sfl rs-parallaxlevel-4" data-x="20" data-y="center" data-voffset="27" data-speed="1000" data-start="2000" data-easing=""><a href="#service" class="btn-round tp-simpleresponsive button blue">SCROLL DOWN</a>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
            <!-- end: Slider -->
        
            <section class="space-sm bg-light" id="service">
               <div class="container">
                  <div class="service-box">
                     <div class="row">
                        <div class="col-md-3 col-sm-3 clearfix cr-nav">
                           <h3 class="f-500">Our Full Service</h3>
                           <p>You can find general information about making appointments, as well as other helpful tips.</p>
                           <div class="customNavigation">
                              <a class="btn btn-default carousel-prev fa fa-long-arrow-left"></a>
                              <a class="btn btn-default carousel-next fa fa-long-arrow-right"></a>
                           </div>
                           <!-- cr.navigation icons:ends -->
                        </div>
                        <!-- .col 3 -->
                        <div class="col-md-9 col-sm-9">
                           <div id="owl-demo" class="row">
                              <div class="item">
                                 <div class="info-block">
                                    <div class="thumbnail">
                                       <figure>
                                          <img src="{!! asset('resources/assets/front') !!}/images/service-9.jpg" alt="" class="img-responsive">
                                       </figure>
                                       <div class="round-icon bg-blue-light">
                                          <span class="icon-i-dental" aria-hidden="true"></span>
                                       </div>
                                       <div class="caption" onclick="location.href='service-details.html';">
                                          <h4>Making Appointments</h4>
                                          <p>You can find general information about making appointments, as well as other helpful tips..</p>
                                       </div>
                                       <ul class="spark-actions">
                                          <li>
                                             <a href="#" data-toggle="tooltip" data-placement="top" title="Find out more about dental health">
                                             <span class="fa fa-info"></span></a>
                                          </li>
                                          <li>
                                             <a href="#">
                                             <span class="fa fa-link"></span></a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 <!-- /.info bock -->
                              </div>
                              <!-- /.tem -->
                              <div class="item">
                                 <div class="info-block">
                                    <div class="thumbnail">
                                       <figure>
                                          <img src="{!! asset('resources/assets/front') !!}/images/service-11.jpg" alt="" class="img-responsive">
                                       </figure>
                                       <div class="round-icon bg-green-light">
                                          <span class="icon-i-genetics" aria-hidden="true"></span>
                                       </div>
                                       <div class="caption" onclick="location.href='service-details.html';">
                                          <h4>Making Appointments</h4>
                                          <p>You can find general information about making appointments, as well as other helpful tips..</p>
                                       </div>
                                       <ul class="spark-actions">
                                          <li>
                                             <a href="#" data-toggle="tooltip" data-placement="top" title="Yes,I'm your info tooltip">
                                             <span class="fa fa-info"></span></a>
                                          </li>
                                          <li>
                                             <a href="#">
                                             <span class="fa fa-link"></span></a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 <!-- /.info bock -->
                              </div>
                              <!-- /.tem -->
                              <div class="item">
                                 <div class="info-block">
                                    <div class="thumbnail">
                                       <figure>
                                          <img src="{!! asset('resources/assets/front') !!}/images/service-8.jpg" alt="" class="img-responsive">
                                       </figure>
                                       <div class="round-icon bg-red-light">
                                          <span class="icon-i-pathology" aria-hidden="true"></span>
                                       </div>
                                       <div class="caption" onclick="location.href='service-details.html';">
                                          <h4>Making Appointments</h4>
                                          <p>You can find general information about making appointments, as well as other helpful tips..</p>
                                       </div>
                                       <ul class="spark-actions">
                                          <li>
                                             <a href="#" data-toggle="tooltip" data-placement="top" title="Yes,I'm your info tooltip">
                                             <span class="fa fa-info"></span></a>
                                          </li>
                                          <li>
                                             <a href="#">
                                             <span class="fa fa-link"></span></a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 <!-- /.info bock -->
                              </div>
                              <!-- /.tem -->
                          
                              <!-- /.tem -->
                 
                              <!-- /.tem -->
                              <div class="item">
                                 <div class="info-block">
                                    <div class="thumbnail">
                                       <figure>
                                          <img src="{!! asset('resources/assets/front') !!}/images/service-7.jpg" alt="" class="img-responsive">
                                       </figure>
                                       <div class="round-icon bg-green">
                                          <span class="icon-i-pathology" aria-hidden="true"></span>
                                       </div>
                                       <div class="caption" onclick="location.href='service-details.html';">
                                          <h4>Clinical Care</h4>
                                          <p>You can find general information about making appointments, as well as other helpful tips..</p>
                                       </div>
                                       <ul class="spark-actions">
                                          <li>
                                             <a href="#" data-toggle="tooltip" data-placement="top" title="Yes,I'm your info tooltip">
                                             <span class="fa fa-info"></span></a>
                                          </li>
                                          <li>
                                             <a href="#">
                                             <span class="fa fa-link"></span></a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 <!-- /.info bock -->
                              </div>
                              <!-- /.tem -->
                           </div>
                        </div>
                     </div>
                     <!--col 9-->
                  </div>
               </div>
               <!-- /.container -->
            </section>
           
            <hr/>
 

    
         <!-- /.tips and news -->
         <div class="cta bg-blue-light">
            <div class="container">
               <div class="row cta-1">
                  <div class="cta-features">
                     <div class="col-sm-3 blue-1"><strong>100% Satisfaction<sub>Guaranteed</sub></strong>
                     </div>
                     <div class="col-sm-3 blue-2"><strong>Free Monitoring<sub>get your healt monitored</sub></strong>
                     </div>
                     <div class="col-sm-3 blue-3"><strong>Get 15% Off<sub>Make an appointment now</sub></strong>
                     </div>
                     <div class="col-sm-3 blue-4"><strong>Call us anytime<sub>+000 123 456 7888</sub></strong>
                     </div>
                  </div>
               </div>
            </div>
         </div>