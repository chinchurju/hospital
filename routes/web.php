<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/**
 * Admin routes
 */
Route::namespace('Admin')->group(function () {
	Route::get('admin', 'LoginController@showLoginForm')->name('admin.login');
    Route::get('admin/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'LoginController@login')->name('admin.login');
    Route::get('admin/logout', 'LoginController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin', 'middleware' => ['employee'], 'as' => 'admin.' ], function () {
    Route::namespace('Admin')->group(function () {
        Route::group(['middleware' => ['role:admin|superadmin|clerk, guard:employee']], function () {
            Route::get('/dashboard/{reportBase}', 'DashboardController@index')->name('dashboard');
         

			//Appointment
			Route::get('/listingAppointment', 'AdminServiceController@listingAppointment')->name('listingAppointment');
			Route::get('/addAppointment', 'AdminServiceController@addAppointment')->name('addAppointment');
			Route::post('/addNewAppointment', 'AdminServiceController@addNewAppointment');
			Route::get('/editAppointment/{id}', 'AdminServiceController@editAppointment');
			Route::post('/updateAppointment', 'AdminServiceController@updateAppointment');
			Route::post('/deleteAppointment/', 'AdminServiceController@deleteAppointment');

			//Brands
			Route::get('/listingDescription', 'AdminDescriptionController@listingDescription')->name('listingDescription');
			Route::get('/addDescription', 'AdminDescriptionController@addDescription')->name('addDescription');
			Route::post('/addNewDescription', 'AdminDescriptionController@addNewDescription');
			Route::get('/editDescription/{id}', 'AdminDescriptionController@editDescription');
			Route::post('/updateDescription', 'AdminDescriptionController@updateDescription');
			Route::post('/deleteDescription/', 'AdminDescriptionController@deleteDescription');


			Route::get('/listingMedicine', 'AdminMedicineController@listingMedicine')->name('listingMedicine');
			Route::get('/addMedicine', 'AdminMedicineController@addMedicine')->name('addMedicine');
			Route::post('/addNewMedicine', 'AdminMedicineController@addNewMedicine');
			Route::get('/editMedicine/{id}', 'AdminMedicineController@editMedicine');
			Route::post('/updateMedicine', 'AdminMedicineController@updateMedicine');
			Route::post('/deleteMedicine/', 'AdminMedicineController@deleteMedicine');
			
			Route::get('/listingTime', 'AdminTimeController@listingTime')->name('listingTime');
			Route::get('/addTime', 'AdminTimeController@addTime')->name('addTime');
			Route::post('/addNewTime', 'AdminTimeController@addNewTime');
			Route::get('/editTime/{id}', 'AdminTimeController@editTime');
			Route::post('/updateTime', 'AdminTimeController@updateTime');
			Route::post('/deleteTime/', 'AdminTimeController@deleteTime');

        });
			Route::group(['middleware' => ['role:admin|superadmin, guard:employee']], function () {
            Route::resource('employees', 'EmployeeController');
            Route::get('employees/{id}/profile', 'EmployeeController@getProfile')->name('employee.profile');
            Route::put('employees/{id}/profile', 'EmployeeController@updateProfile')->name('employee.profile.update');
            Route::resource('roles', 'Roles\RoleController');
            Route::resource('permissions', 'Permissions\PermissionController');
        });
    });
});

/**
 * Frontend routes
 */
Auth::routes();
Route::namespace('Auth')->group(function () {
    Route::get('cart/login', 'CartLoginController@showLoginForm')->name('cart.login');
    Route::post('cart/login', 'CartLoginController@login')->name('cart.login');
    Route::get('logout', 'LoginController@logout');
});

Route::namespace('Front')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
	Route::get("contact-us", 'ContactusController@index')->name('contactus.index');
	Route::post("contact-us", 'ContactusController@send')->name('contactus.send');
    //Route::get("login", 'HomeController@login')->name('login');
	//Route::post("loginn", 'HomeController@loginn')->name('loginn');
	Route::get('refreshcaptcha', 'HomeController@refreshCaptcha');
	Route::get("registration", 'HomeController@registration')->name('registration');
	Route::post("register", 'HomeController@register')->name('register');
	Route::get("appointments", 'HomeController@appointments')->name('appointment');
	Route::get("new-appointment", 'HomeController@newAppointments')->name('appointment_new');
	Route::post("new-appointment", 'HomeController@saveNewAppointments')->name('appointment_new');	
	Route::get("client", 'HomeController@client')->name('client');
	Route::get("app-profile", 'HomeController@appprofile')->name('app-profile');
	Route::get("medicine", 'HomeController@medicine')->name('medicine');
	Route::get("deceased", 'HomeController@deceased')->name('deceased');
	Route::get('generate-pdf','HomeController@generatePDF')->name('generatePDF');
	Route::get('generatemedicine-pdf','HomeController@generatemedicinePDF')->name('generatemedicinePDF');
	Route::get('generatedes-pdf','HomeController@generatedesPDF')->name('generatedesPDF');

});