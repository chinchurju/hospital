<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Al Saqr</title>
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/styles/bootstrap4/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css">

		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/plugins/OwlCarousel2-2.2.1/animate.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/plugins/slick-1.8.0/slick.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/styles/about_styles.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/styles/about_responsive.css">
	</head>
	<body>
		<?php echo $__env->make('layouts.front.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		<?php echo $__env->yieldContent('content'); ?>

		<?php echo $__env->make('layouts.front.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/js/jquery-3.2.1.min.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/styles/bootstrap4/popper.js"></script>
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/styles/bootstrap4/bootstrap.min.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/plugins/greensock/TweenMax.min.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/plugins/greensock/TimelineMax.min.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/plugins/scrollmagic/ScrollMagic.min.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/plugins/greensock/animation.gsap.min.js"></script>
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/plugins/greensock/ScrollToPlugin.min.js"></script>
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/plugins/slick-1.8.0/slick.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/plugins/scrollTo/jquery.scrollTo.min.js"></script> 
		<?php if(Route::currentRouteName()!="aboutus"): ?>
<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/js/custom.js"></script>
		
		 <?php endif; ?>
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/plugins/easing/easing.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/js/about_custom.js"></script>
	</body>
</html>
