<?php $__env->startSection('content'); ?>
<section class="content">
  <div class="box">
    <div class="box-body">
      <h2>Add Review</h2>
      <div class="box-body">
		    <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box-body">
                      <?php if( count($errors) > 0): ?>
                      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        <?php echo e($error); ?>

                      </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>
                       <?php echo Form::open(array('url' =>'admin/addNewMedicine', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')); ?>

                      
							        <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Description Id</label>
                        <div class="col-sm-10 col-md-4">
						
							<select class="form-control" name="desid" id="desid">
												<option value="">Select </option>
												<?php if($dsriptn->count() > 0): ?>
													<?php $__currentLoopData = $dsriptn; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														<option value="<?php echo e($row->id); ?>"><?php echo e($row->id); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
												<?php endif; ?>
											
											</select>
						
                         
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Medicine</label>
                        <div class="col-sm-10 col-md-4">
                         <input type="text" name="medicine" class="form-control field-validate">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Medicine Description</label>
                        <div class="col-sm-10 col-md-8">
                          <textarea id="editor" name="medicinedesc" class="form-control" rows="10" cols="80"></textarea> <br>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Time</label>
                        <div class="col-sm-10 col-md-4">
                          <tr> 
                            <td><input type="checkbox" name="time[]" class="mode1 mode_checkbox_mode" data-id="" value="Morning"></td>
                            <td>Morning</td>
                          </tr>
                          <tr> 
                            <td><input type="checkbox" name="time[]" class="mode1 mode_checkbox_mode" data-id="" value="Afternoon"></td>
                            <td>Afternoon</td>
                          </tr>
                          <tr> 
                            <td><input type="checkbox" name="time[]" class="mode1 mode_checkbox_mode" data-id="" value="Night"></td>
                            <td>Night</td>
                          </tr>
                        </div>
                      </div>

                    
        							<div class="box-footer text-center">
        								<button type="submit" class="btn btn-primary"><?php echo e(trans('labels.SubmitNews')); ?></button>
        								<a href="<?php echo e(URL::to('admin/listingMedicine')); ?>" type="button" class="btn btn-default"><?php echo e(trans('labels.back')); ?></a>
        							</div>
                       <?php echo Form::close(); ?>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> 
	</div>
</section>
<script src="<?php echo asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>
<script type="text/javascript">
		$(function () {
			$("textarea").summernote({height: "400px",});
    });
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>