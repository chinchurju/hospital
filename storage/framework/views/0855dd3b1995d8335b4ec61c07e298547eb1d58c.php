<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.front.home-slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="icon_boxes">
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<div class="icon_box_title">
					<h1>Security Solutions with over 7 years of experience </h1>
				</div>
				<div class="button icon_box_button trans_200">
					<a href="#" class="trans_200">Know more</a>
				</div>
			</div>
			<div class="col-lg-7 icon_box_col">
				<div class="icon_box_item">
					<h2>About us: </h2>
					<p>AlSAQR is a Security Solutions Provider company with over 7 years of experience in the Security Systems Industry.
					We provide full - Security Solutions for Commercial, Residential Places with Alarm Systems, Video Door Phone, CCTV Analog, CCTV IP Based, Card Fingerprint Time Attendance, Door Access Control Systems and Fire Alarms.
					Combining our business experience, technical expertise, profound knowledge of latest industry trends and quality-driven delivery model we offer progressive end-to-end Solutions.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="services">
	<div class="container">
		<div class="row">
			<div class="col text-center">
				<div class="section_title">
					<h1>We Offer Progressive end-to-end Solutions</h1>
					<span>Explore our Products</span>
				</div>
			</div>
		</div>
	</div>
	<div class="h_slider_container services_slider_container">
		<div class="service_slider_outer">
		  <?php if(!$service->isEmpty()): ?>
			<div class="owl-carousel owl-theme services_slider">
			 <?php $__currentLoopData = $service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $services): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="owl-item services_item">
					<div class="services_item_inner">
						<div class="service_item_content">
							<div class="service_item_title">
								<div class="service_item_icon">
									<img src="<?php echo $services->shortimage; ?>" >
								</div>
								<h2><?php echo $services->name; ?></h2>
							</div>
							<p><?php echo $services->shortdes; ?></p>
							<div class="button service_item_button trans_200">
								<a href="#" class="trans_200">discover more</a>
							</div>
						</div>
					</div>
				</div>
			 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		 <?php endif; ?>
			<div class="services_slider_nav services_slider_nav_left"><i class="fas fa-chevron-left trans_200"></i></div>
			<div class="services_slider_nav services_slider_nav_right"><i class="fas fa-chevron-right trans_200"></i></div>
		</div>
	</div>
</div>
<div class="features">
	<div class="container">
		<div class="row">
			<div class="col text-center">
				<div class="section_title">
					<h1>Our strength</h1>
				</div>
			</div>
		</div>
		<div class="row features_row">
			<div class="col-lg-4 text-lg-right features_col order-lg-1 order-2">
				<div class="features_item">
					<h2>Our Business philosophy</h2>
					<p>ALSAQR Philosophy is not selling the product, knowing the customer needs providing a perfect solution making a business relationship.</p>
				</div>
				<div class="features_item">
					<h2>Experience & Expertise</h2>
					<p>Having worked on numerous projects,  ALSAQR has gained unmatched business and technological expertise.</p>
				</div>
			</div>
			<div class="col-lg-4 d-flex flex-column align-items-center order-lg-2 order-1">
				<div class="features_image">
					<img src="<?php echo asset('resources/assets/front'); ?>/images/strength.png" alt="">
				</div>
				<div class="button features_button trans_200">
					<a href="#" class="trans_200">Know More</a>
				</div>
			</div>
			<div class="col-lg-4 features_col order-lg-3 order-3">
				<div class="features_item">
					<h2>Commitment to Quality</h2>
					<p>While retaining competitive rates we never compromise the quality of our services. A dedicated quality assurance department...</p>
				</div>
				<div class="features_item">
					<h2>Customer Focused approach</h2>
					<p>ALSAQR is a client-centric organization. We make it our business to understand and help our clients to achieve their business goals.</p>
				</div>
				<div class="button features_button_2 trans_200">
					<a href="#" class="trans_200">discover more</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.apphome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>