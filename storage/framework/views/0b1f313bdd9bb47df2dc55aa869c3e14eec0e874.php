<?php $__env->startSection('content'); ?>
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Edit Details</h2>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
              		<div class="box box-info">
                    <br>                       
                        <?php if(count($errors) > 0): ?>
                              <?php if($errors->any()): ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <?php echo e($errors->first()); ?>

                                </div>
                              <?php endif; ?>
                          <?php endif; ?>
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit category</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->  
						<div class="panel with-nav-tabs  panel-default">
							<!--<div class="panel-heading" style="border:none;">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#English" data-toggle="tab">English</a></li>
										<li><a href="#Arabic" data-toggle="tab">Arabic</a></li>
									</ul>
							</div>-->
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="English">
						
										 <div class="box-body">
										 
											<?php echo Form::open(array('url' =>'admin/updateMedicine', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>

											  
											<?php echo Form::hidden('id',  $result['review'][0]->id , array('class'=>'form-control', 'id'=>'id')); ?>


												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Register Id</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('desid', $result['review'][0]->desid, array('class'=>'form-control','id'=>'desid','readonly')); ?>

												  </div>
												</div>
												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Medicine</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('medicine', $result['review'][0]->medicine, array('class'=>'form-control ','id'=>'medicine')); ?>

												  </div>
												</div>
												<div class="form-group">
					                                <label for="name" class="col-sm-2 col-md-3 control-label">Medicine Description</label>
					                                <div class="col-sm-10 col-md-8">
					                                  <textarea id="editor" name="medicinedesc" class="form-control" rows="10" cols="80"><?php echo $result['review'][0]->medicinedesc; ?></textarea> <br>
					                                </div>
				                             	</div>

												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Time</label>
												  <div class="col-sm-10 col-md-4">
													  <tr> 
							                            <td><input type="checkbox" name="time[]" class="mode1 mode_checkbox_mode" data-id="" value="Morning"></td>
							                            <td>Morning</td>
							                          </tr>
							                           <tr> 
							                            <td><input type="checkbox" name="time[]" class="mode1 mode_checkbox_mode" data-id="" value="Afternoon"></td>
							                            <td>Afternoon</td>
							                          </tr>
							                          <tr> 
							                            <td><input type="checkbox" name="time[]" class="mode1 mode_checkbox_mode" data-id="" value="Night"></td>
							                            <td>Night</td>
							                          </tr>
												  </div>
												</div>
											  
												
											  <!-- /.box-body -->
											  <div class="box-footer text-center">
												<button type="submit" class="btn btn-primary"><?php echo e(trans('labels.Update')); ?></button>
												<a href="<?php echo e(URL::to('admin/listingMedicine')); ?>" type="button" class="btn btn-default"><?php echo e(trans('labels.back')); ?></a>
											  </div>
											  <!-- /.box-footer -->
											<?php echo Form::close(); ?>

										</div>
									</div>
							
						
                  </div>
              </div>
            </div>
            
          </div>
         <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
   
  </section>
  <!-- /.content --> 

   <script src="<?php echo asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>


<script type="text/javascript">
		$(function () {
			
			//bootstrap WYSIHTML5 - text editor
			//
			$("textarea").summernote({height: "400px",});
			//$("textarea").wysihtml5();
			
    });
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>