<header class="inner_head">
	<div class="wsmenucontainer clearfix">
    <div id="overlapblackbg"></div>
    <div class="wsmobileheader clearfix"> <a id="wsnavtoggle" class="animated-arrow"><span></span></a> <a href="index.html" class="smallogo"><img src="<?php echo asset('resources/assets/front'); ?>/images/logo.png"></a> <a class="callusicon" href="tel:123456789"><span class="fa fa-phone"></span></a> </div>
    <div class="header">
      <!--Main Menu HTML Code-->
      <div class="wsmain">
        <div class="smllogo"><a href="<?php echo e(route('home')); ?>"><img src="<?php echo asset('resources/assets/front'); ?>/images/logo.png"></a></div>
        <nav class="wsmenu clearfix">
          <ul class="mobile-sub wsmenu-list">
            <li><a href="<?php echo e(route('home')); ?>">HOME  </a></li>
            <li><a href="#">About Us <span class="arrow"></span></a>
              <div class="megamenu clearfix halfmenu">
                <div class="container-fluid">
                  <div class="row">
                    <ul class="col-lg-5 col-md-12 col-xs-12 link-list">
                      <img src="<?php echo asset('resources/assets/front'); ?>/images/logo.png" class="img-fluid">
                    </ul>
                    <ul class="col-lg-7 col-md-12 col-xs-12 link-list">
                      <li><a href="<?php echo e(route('aboutus')); ?>"><i class="fa fa-angle-right"></i>About Company</a></li>
                      <li><a href="<?php echo e(route('tributes')); ?>"><i class="fa fa-angle-right"></i>Tribute</a></li>
                      <li><a href="<?php echo e(route('board')); ?>"><i class="fa fa-angle-right"></i>Board of Directors</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li><a href="<?php echo e(route('company')); ?>">COMPANIES  </a></li>
            <li><a href="<?php echo e(route('news')); ?>">NEWS & EVENTS  </a></li>
            <li><a href="<?php echo e(route('careers')); ?>">CAREERS  </a></li>
            <li><a href="<?php echo e(route('contactus.index')); ?>">CONTACT US  </a></li>
          </ul>
        </nav>
        <div class='control'>
          <div class='btn-material'></div>
          <i class='flaticon-magnifying-glass'></i>
        </div>
        <i class='icon-close flaticon-close'></i>
        <div class='search-input'>
           <?php echo Form::open(array('url' =>'search', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data','id'=>'searchform')); ?>

            <input class='input-search' placeholder='Start Typing' name="q" type='text' id='searchtext'>
          <?php echo Form::close(); ?>

        </div>
      </div>
      <!--Menu HTML Code-->
    </div>
  </div>
</header>