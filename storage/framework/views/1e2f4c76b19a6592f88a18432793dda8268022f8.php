<?php $__env->startSection('content'); ?>
<section class="abt-tp">
	<div class="container">
		<div class="col-md-12">
			<h3>About AL RABBAN Holding</h3>
			<ul class="breadcrumb">
			  <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
			  <li class="breadcrumb-item active">About al rabban Holding</li>
			</ul>
		</div>
	</div>
</section>

<section class="abt-sec">
	<div class="container">
		<div class="col-md-12">
			<h3>AL RABBAN STORY</h3>
			<h5>The Rabban Group of Companies was formed in 1964 and is amongst the oldest groups in Doha, State of Qatar.  <div class="scroll-down"><div class="scroll-down__line"></div></div></h5>
			<div class="row">
				<div class="col-md-6" data-aos="fade-up">
					<h4>The controlling shareholder and Chairman is Mr. Khalid bin Moh’d Al Rabban, a prominent local businessman who also has extensive family real estate interests in the Doha area.</h4>
					<h4>A significant achievement for the group as a whole has been the award of ISO certification to the operating units. These awards reflect the commitment made by the group to professional management and are a reflection of the on-going development within the overall organisation.</h4>
					<h4>The group’s intentions remain unchanged – to play a leading role in the on-going development of the country, and to work within this development in providing the best possible quality of product and service to clients and consumers alike.</h4>
					<h4>We are also committed to the further expansion of the group, and to encourage personal development within the group by providing an environment where individual talents and efforts are properly rewarded. </h4>
				</div>
				<div class="col-md-6" data-aos="fade-up">
					<h4>The group’s principle operating units are:</h4>
					<ul>
						<li>Rayyan Water Company</li>
						<li>Jannah Water Company</li>
						<li>Polytech Plastic Industries</li>
						<li>Al Sarh Real Estate Company</li>
						<li>Al Rabban Tower</li>
						<li>Al Rabban Hospitality</li>
						<li>Fraser Suites Westbay Doha</li>
						<li>Rabban Readymix Company</li>
						<li>Arabian Falcon Transport Company</li>
						<li>Rabban Contracting & Trading Company</li>
						<li>Al Rabban Agriculture Company</li>
						<li>Rabban Services Company</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>