    <div class="animsition">
        <div class="wrapper">
        <header id="header">
                <!-- Top Header Section Start -->
                <div class="top-bar bg-light hdden-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 list-inline list-unstyled no-margin hidden-xs">
                                <p class="no-margin">Have any questions? <strong>+080 124 880</strong> or mail@modernclinic.com</p>
                            </div>
                            <div class="btn-group pull-right col-sm-6">
                                <button class="dropdown-toggle language" type="button" data-toggle="dropdown" aria-expanded="false">
                                    <img src="<?php echo asset('resources/assets/front'); ?>/images/us.png" width="16" height="11" alt="EN Language">English <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu no-border-radius no-shadow">
                                    <li>
                                        <a href="#">
                                            <img src="<?php echo asset('resources/assets/front'); ?>/images/us.png" width="16" height="11" alt="EN Language">[US] English
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="<?php echo asset('resources/assets/front'); ?>/images/de.png" width="16" height="11" alt="DE Language">[DE] German
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="<?php echo asset('resources/assets/front'); ?>/images/fr.png" width="16" height="11" alt="FR Language">[FR] French
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="<?php echo asset('resources/assets/front'); ?>/images/ru.png" width="16" height="11" alt="RU Language">[RU] Russian
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.top bar -->
                <!-- begin Logo and info -->
                <div class="container middle-bar hidden-xs">
                    <div class="row">
                        <a href="<?php echo e(route('home')); ?>" class="logo col-sm-3">
                <img src="http://localhost/party/images/logo-dark.png" class="img-responsive" alt="" />
                        </a>
                        <div class="col-sm-8 col-sm-offset-1 contact-info">
                              <p><a href="<?php echo e(route('registration')); ?>" class="btn btn-primary" role="button">Book Appointment</a> <a href="<?php echo e(route('login')); ?>" class="btn btn-primary" role="button">User Login </a>
                                <a href="http://localhost/modernclinic/admin/login" class="btn btn-primary" role="button">Admin Login</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.middle -->
                <!-- begin MegaNavbar-->
                <div class="nav-wrap-holder">
                    <div class="container" id="nav_wrapper">
                        <nav class="navbar navbar-static-top navbar-default no-border-radius xs-height100" id="main_navbar" role="navigation">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#MegaNavbarID">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                                    </button>
                                    <a href="<?php echo e(route('registration')); ?>" class="navbar-brand logo col-sm-3 visible-xs-block">
                                        
                                    </a>
                                </div>
                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="MegaNavbarID">
                                    <!-- regular link -->
                                    <ul class="nav navbar-nav navbar-left">
                                        <li><a href="<?php echo e(route('home')); ?>"><i class="fa fa-home"></i> <span class="hidden-sm">Home</span></a>
                                        </li>
                                        <li class="divider"></li>
                                        <li class="dropdown-full no-shadow no-border-radius">
                                            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><span class="icon-i-family-practice" aria-hidden="true"></span>&nbsp;<span class="hidden-sm hidden-md reverse">Pages</span><span class="caret"></span></a>
                                            <div class="dropdown-menu row drop-image">
                                                <ul class="row">
                                                    <li class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                                        <ul>
                                                            <li class="dropdown-header">Service pages</li>
                                                            <li><a href="service-details.html">Service details</a>
                                                            </li>
                                                            <li><a href="service.html">Service list</a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li class="dropdown-header">Extra pages</li>
                                                            <li><a href="timetable.html">jQuery timetable page</a>
                                                            </li>
                                                            <li><a href="pricing.html">Pricing</a>
                                                            </li>
                                                            <li><a href="contact.html">Contact</a>
                                                            </li>
                                                            <li><a href="faq.html">FAQ page</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                                        <ul>
                                                            <li class="dropdown-header">Template features</li>
                                                            <li><a href="icons.html">Medical Icons</a>
                                                            </li>
                                                            <li><a href="features.html">Elements</a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li class="dropdown-header">Blog pages</li>
                                                            <li><a href="blog-post.html">Blog post</a>
                                                            </li>
                                                            <li><a href="blog.html">Blog</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                                        <ul>
                                                            <li class="dropdown-header">Info pages</li>
                                                            <li><a href="about_us.html">About us page</a>
                                                            </li>
                                                            <li><a href="about_2.html">Team page</a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li class="dropdown-header">Newsletter</li>
                                                            <li>
                                                                <form class="form" role="form">
                                                                    <div class="form-group">
                                                                        <label class="sr-only" for="email">Email address</label>
                                                                        <input type="email" class="form-control" id="e-mail" placeholder="Enter email">
                                                                    </div>
                                                                    <button type="submit" class="btn btn-primary btn-block btn-fill">Sign in</button>
                                                                </form>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="col-xs-6 col-sm-3 col-md-3 col-lg-3 hidden-xs">
                                                        <figure>
                                                            <img src="http://placehold.it/400x600" class="img-responsive menu-img" alt="">
                                                        </figure>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="divider"></li>
                                        <!-- dropdown active -->
                                        <li class="dropdown-full no-border-radius no-shadow">
                                            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-money"></i>&nbsp;<span class="hidden-sm hidden-md reverse">Pricing</span><span class="caret"></span></a>
                                            <div class="dropdown-menu no-padding HingeUpToDown">
                                                <ul id="myTab" style="margin-top: 1px;">
                                                    <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6 no-padding active"><a href="#1" data-toggle="tab">Dental care</a>
                                                    </li>
                                                    <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6 no-padding"><a href="#2" data-toggle="tab">Mental health</a>
                                                    </li>
                                                    <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6 no-padding"><a href="#3" data-toggle="tab">Gnathological treatment</a>
                                                    </li>
                                                    <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6 no-padding"><a href="pricing.html">View All Prices</a>
                                                    </li>
                                                </ul>
                                                <div id="myTabContent-1" class="tab-content">
                                                    <div class="tab-pane active" id="1">
                                                        <ul class="row">
                                                            <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown-header text-center" style="margin: 0 -1px; width: calc(100% + 2px); padding:0 15px;">
                                                                <h4 class="bg-blue-light">
                                                      You can find our prices here. For a personalized calculation please Contact us
                                                   </h4>
                                                            </li>
                                                        </ul>
                                                        <div class="divided">
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€345</span>
                                                                        <span class="text"><strong>Consultation</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€124</span>
                                                                        <span class="text"><strong>Digital X-Ray</strong></span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>CT Scan</strong></span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Gnathological treatment </strong></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€345</span>
                                                                        <span class="text"><strong>Dental Implants</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€124</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Veneers</strong> from</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€345</span>
                                                                        <span class="text"><strong>Dental Implants</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€124</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Veneers</strong> from</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€345</span>
                                                                        <span class="text"><strong>Dental Implants</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€124</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Veneers</strong> from</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="2">
                                                        <ul class="row">
                                                            <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown-header text-center" style="margin: 0 -1px; width: calc(100% + 2px); padding:0 15px;">
                                                                <h4 class="bg-green color-white">
                                                      You can find our prices here. For a personalized calculation please Contact us
                                                   </h4>
                                                            </li>
                                                        </ul>
                                                        <div class="divided">
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€95</span>
                                                                        <span class="text"><strong>Paediatric</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€200</span>
                                                                        <span class="text"><strong>Genetic</strong></span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>CT Scan</strong></span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Gnathological treatment </strong></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€345</span>
                                                                        <span class="text"><strong>Dental Implants</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€124</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Veneers</strong> from</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€345</span>
                                                                        <span class="text"><strong>Dental Implants</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€124</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Veneers</strong> from</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€345</span>
                                                                        <span class="text"><strong>Dental Implants</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€124</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Veneers</strong> from</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane " id="videohive">
                                                        <ul class="row">
                                                            <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown-header text-center" style="margin: 0 -1px; width: calc(100% + 2px); padding:0 15px;">
                                                                <h4 class="bg-red color-white">
                                                      You can find our prices here. For a personalized calculation please Contact us
                                                   </h4>
                                                            </li>
                                                        </ul>
                                                        <div class="divided">
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€345</span>
                                                                        <span class="text"><strong>Consultation</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€124</span>
                                                                        <span class="text"><strong>Digital X-Ray</strong></span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>CT Scan</strong></span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Gnathological treatment </strong></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€345</span>
                                                                        <span class="text"><strong>Dental Implants</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€124</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Veneers</strong> from</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€345</span>
                                                                        <span class="text"><strong>Dental Implants</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€124</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Veneers</strong> from</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <ul class="category-procedures col-sm-3 col-xs-6 col-md-3 h-divided">
                                                                <li class="first">
                                                                    <a href="#">
                                                                        <span class="price">€345</span>
                                                                        <span class="text"><strong>Dental Implants</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€124</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="price">€160</span>
                                                                        <span class="text"><strong>Crowns</strong> from</span>
                                                                    </a>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#">
                                                                        <span class="price">€29</span>
                                                                        <span class="text"><strong>Veneers</strong> from</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- divider -->
                                        <li class="divider"></li>
                                        <!-- dropdown short. Forced show on small devices on hover  -->
                                        <li class="dropdown-short">
                                            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-bars"></i>&nbsp;<span class="hidden-sm hidden-md reverse">Dropdown menu</span><span class="caret"></span></a>
                                            <ul class="dropdown-menu no-border-radius no-shadow">
                                                <li class="divider no-margin"></li>
                                                <li class="dropdown-right-onclick">
                                                    <!-- Menu item with submenu -->
                                                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#id_1" class="dropdown-toggle collapsed"><i class="fa fa-bars"></i> With submenu<span class="desc">Drop down on click to the right</span></a>
                                                    <!-- start submenu -->
                                                    <ul class="dropdown-menu no-shadow collapse" id="id_1">
                                                        <li class="divider"></li>
                                                        <li class="dropdown-right-onclick">
                                                            <!-- Menu item with submenu -->
                                                            <a href="javascript:void(0);" data-toggle="collapse" data-target="#id_2" class="dropdown-toggle collapsed"><i class="fa fa-bars"></i> With submenu<span class="desc">Drop down on click to the right</span></a>
                                                            <!-- start submenu -->
                                                            <ul class="dropdown-menu no-border-radius no-shadow collapse" id="id_2">
                                                                <li class="dropdown-header">Submenu header</li>
                                                                <li class="divider"></li>
                                                                <li><a href="javascript:void(0);" class="">Default item<span class="desc">Default item description</span></a>
                                                                </li>
                                                                <li class="active"><a href="javascript:void(0);" class="">Active item<span class="desc">Active item description</span></a>
                                                                </li>
                                                                <li><a href="javascript:void(0);" class="">Disabled item<span class="desc">Disabled item description</span></a>
                                                                </li>
                                                                <li><a href="javascript:void(0);" class="">Open item<span class="desc">Open item description</span></a>
                                                                </li>
                                                                <li class="divider"></li>
                                                                <li><a href="javascript:void(0);">Separated link</a>
                                                                </li>
                                                            </ul>
                                                            <!-- end submenu -->
                                                        </li>
                                                        <li><a href="javascript:void(0);" class="">Default item<span class="desc">Default item description</span></a>
                                                        </li>
                                                        <li class="active"><a href="javascript:void(0);" class="">Active item<span class="desc">Active item description</span></a>
                                                        </li>
                                                        <li><a href="javascript:void(0);" class="">Disabled item<span class="desc">Disabled item description</span></a>
                                                        </li>
                                                        <li><a href="javascript:void(0);" class="">Open item<span class="desc">Open item description</span></a>
                                                        </li>
                                                        <li class="divider"></li>
                                                        <li><a href="javascript:void(0);">Separated link</a>
                                                        </li>
                                                    </ul>
                                                    <!-- end submenu -->
                                                </li>
                                                <li class="no-margin divider"></li>
                                                <li><a href="javascript:void(0);">Default item<span class="desc">Default item description</span></a>
                                                </li>
                                                <li class="active"><a href="javascript:void(0);" class="">Active item<span class="desc">Active item description</span></a>
                                                </li>
                                                <li class="disabled"><a href="javascript:void(0);" class="">Disabled item<span class="desc">Disabled item description</span></a>
                                                </li>
                                                <li class="open"><a href="javascript:void(0);" class="">Open item<span class="desc">Open item description</span></a>
                                                </li>
                                                <li class="divider"></li>
                                            </ul>
                                        </li>
                                        <!-- divider -->
                                        <li class="divider"></li>
                                    </ul>
                                    <ul class="nav navbar-nav navbar-right">
                                        <li>
                                            <!-- search form -->
                                            <form class="navbar-form-expanded navbar-form navbar-left visible-lg-block visible-md-block visible-xs-block" role="search">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" data-width="80px" data-width-expanded="170px" placeholder="Search" name="query">
                                                    <span class="input-group-btn"><button class="btn btn-default" type="submit"><i class="fa fa-search"></i>&nbsp;</button></span>
                                                </div>
                                            </form>
                                        </li>
                                        <li class="dropdown-grid visible-sm-block">
                                            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-search"></i>&nbsp;</a>
                                            <div class="dropdown-grid-wrapper" role="menu">
                                                <ul class="dropdown-menu col-sm-6">
                                                    <li>
                                                        <form class="no-margin">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control">
                                                                <span class="input-group-btn"><button class="btn btn-default" type="submit">&nbsp;<i class="fa fa-search"></i></button></span>
                                                            </div>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="divider"></li>
                                        <li class="dropdown-grid">
                                            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-tasks"></i>&nbsp;<span class="hidden-sm hidden-md reverse">Media</span><span class="caret"></span></a>
                                            <div class="dropdown-grid-wrapper" role="menu">
                                                <ul class="dropdown-menu no-border-radius no-shadow col-xs-12 col-sm-9 col-md-8 col-lg-7">
                                                    <li>
                                                        <div id="carousel-eg" class="carousel slide" data-ride="carousel">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 divided">
                                                                    <ol class="carousel-indicators navbar-carousel-indicators h-divided">
                                                                        <li data-target="#carousel-eg" data-slide-to="0" class="active"><a href="javascript:void(0);" class="">Vimeo<span class="hidden-sm hidden-xs desc">embed your vimeo videos</span></a>
                                                                        </li>
                                                                        <li data-target="#carousel-eg" data-slide-to="1" class=""><a href="javascript:void(0);" class="">Youtube<span class="hidden-sm hidden-xs desc">any youtube video</span></a>
                                                                        </li>
                                                                        <li data-target="#carousel-eg" data-slide-to="2" class=""><a href="javascript:void(0);" class="">Image<span class="hidden-sm hidden-xs desc">image showcase</span></a>
                                                                        </li>
                                                                        <li data-target="#carousel-eg" data-slide-to="3" class=""><a href="javascript:void(0);" class="">Image<span class="hidden-sm hidden-xs desc">Short description</span></a>
                                                                        </li>
                                                                    </ol>
                                                                </div>
                                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                                    <div class="carousel-inner">
                                                                        <div class="item active">
                                                                            <div class="responsive-video">
                                                                                <iframe src="//player.vimeo.com/video/77534721" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item">
                                                                            <div class="responsive-video">
                                                                                <iframe src="//player.vimeo.com/video/77534721" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item">
                                                                            <div class="embed-responsive embed-responsive-16by9">
                                                                                <img class="embed-responsive-item" src="http://placehold.it/400x300" alt="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="item">
                                                                            <div class="embed-responsive embed-responsive-16by9">
                                                                                <img class="embed-responsive-item" src="http://placehold.it/400x300" alt="">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="divider"></li>
                                        <!-- dropdown grid -->
                                        <li class="dropdown-full">
                                            <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-envelope"></i>&nbsp;<span class="hidden-sm hidden-md hidden-lg reverse">Account</span><span class="caret"></span></a>
                                            <ul class="dropdown-menu no-shadow no-border-radius space-xs">
                                                <li class="col-sm-4">
                                                    <address>
                                             <br>
                                             <strong>Brand, Inc.</strong><br>
                                             123 Folsom Ave, Suite 600<br>
                                             San Francisco, CA 94107<br>
                                             <abbr title="Phone">Phone:</abbr> (123) 456-7890<br>
                                             <abbr title="Mobile Phone">Mobile:</abbr> (098) 765-4321
                                          </address>
                                                    <address>
                                             <strong>Full Name</strong><br>
                                             <a href="mailto:#">first.last@example.com</a>
                                          </address>
                                                    <div class="open-hours">
                                                        <p>Monday - Friday <span>8.00 - 17.00</span>
                                                        </p>
                                                        <p>Saturday <span>9.30 - 17.30</span>
                                                        </p>
                                                        <p>Sunday <span>9.30 - 15.00</span>
                                                        </p>
                                                    </div>
                                                </li>
                                                <li class="col-sm-7 col-sm-offset-1">
                                                    <div id="message"></div>
                                                    <form method="post" action="contact.php" name="contactform" id="contactform">
                                                        <fieldset>
                                                            <input class="form-control" name="name" type="text" id="name" size="30" value="" placeholder="Name" />
                                                            <br />
                                                            <input class="form-control" name="email" type="text" id="email" size="30" value="" placeholder="E-mail" />
                                                            <br />
                                                            <input class="form-control" name="phone" type="text" id="phone" size="30" value="" placeholder="Phone" />
                                                            <br />
                                                            <select class="form-control" name="subject" id="subject">
                                                                <option value="0" selected="selected">Select your reason</option>
                                                                <option value="1">Appointment</option>
                                                                <option value="2">Support</option>
                                                                <option value="3">Emergency</option>
                                                            </select>
                                                            <br />
                                                            <textarea class="form-control" name="comments" cols="40" rows="3" id="comments" style="width: 100%;" placeholder="Your comments"></textarea>
                                                            <p><span class="required">*</span> Are you human?</p>
                                                            <label for="verify" accesskey="V">&nbsp;&nbsp;&nbsp;3 + 1 =</label>
                                                            <input name="verify" type="text" id="verify" size="4" value="" style="width: 30px; border:1px solid #eee;" />
                                                            <br />
                                                            <br />
                                                            <input type="submit" class="btn btn-primary btn-fill" id="submit" value="Submit" />
                                                        </fieldset>
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- /.div nav wrap holder -->
            </header>