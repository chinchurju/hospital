    <div class="animsition">
        <div class="wrapper">
            <header id="header">
                <!-- Top Header Section Start -->
                <div class="top-bar bg-light hdden-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 list-inline list-unstyled no-margin hidden-xs">
                                <p class="no-margin">Have any questions? <strong>1234567890</strong> or mail@medican.com</p>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.top bar -->
                <!-- begin Logo and info -->
                <div class="container middle-bar hidden-xs">
                    <div class="row">
                        <a href="<?php echo e(route('home')); ?>" class="logo col-sm-3">
                            <img src="<?php echo asset('resources/assets/front'); ?>/images/logo-dark.png" class="img-responsive" alt="" />
                        </a>
                        <div class="col-sm-8 col-sm-offset-1 contact-info">
                            
                        </div>
                    </div>
                </div>

            </header>