<?php $__env->startSection('content'); ?>
<!--  <div class="fullsize-container">
                <div class="fullsize-slider">
                    <ul>
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="http://placehold.it/200x200" data-delay="7000" data-saveperformance="off" data-title="Slide" style="color: white;">
                            <img src="<?php echo asset('resources/assets/front'); ?>/images/banner1.png" alt="fullslide6" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                           
                        </li>
                    </ul>
                </div>
            </div>  -->
  				<div class="container space-sm">
                    <div class="col-md-12">
					 <?php echo $__env->make('layouts.errors-and-messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <div class="book-box row">
                            <div class="book-form">
                                <h4 class="hr-after">Make an appointment</h4>
							 <?php echo Form::open(array('url' =>'register', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data', 'id' => 'sub')); ?>

                                <div class="row">
                                    <p class="col-md-6 col-sm-6">
                                        <input class="form-control" name="name" type="text" placeholder="Name" required="">
                                    </p>
                                    <p class="col-md-6 col-sm-6">
                                        <input class="form-control" type="text" name="address" placeholder="Address" required="">
                                    </p>
                                </div>

								
								<div class="row">
                              <p class="col-md-6 col-sm-6">
										<input class="form-control" type="date" name="dob" placeholder="Date of Birth" required="">
									</p>
								 <p class="col-md-6 col-sm-6">
                                    <input class="form-control" type="text" name="place" placeholder="Place" required="">
                                </p>
								</div>
								
							
									<div class="row" style="margin-left: 20px;">
									<p class="col-md-6 col-sm-6">
										<input id="radio-1" name="gender" type="radio" value="female" required="required" checked="checked">
										<label for="radio-1" class="radio-label">Female</label>
										<input id="radio-2" name="gender" type="radio" value="male" required="required">
										<label for="radio-2" class="radio-label">Male</label>
									</p>
									<p class="col-md-6 col-sm-6">	
									
										<select class="form-control" name="timee" id="timee" style="margin-left: -17px;width: 103%;">
											<option value="">Sunday Holiday</option>
											<option value="5pm-6pm">5pm to 6pm</option>
											<option value="6pm-7pm">6pm to 7pm</option>
											<option value="7pm-8pm">7pm to 8pm</option>
											<option value="8pm-9pm">8pm to 9pm</option>
										</select>
									</p>									
									</div>

								
								<div class="row">
									<p class="col-md-6 col-sm-6">
										<input class="form-control" type="text" name="phone" placeholder="Phone Number" required="" onKeyPress="return isNumberKey(event)" maxlength="11">
									</p>
									<p class="col-md-6 col-sm-6">
										<input class="form-control" type="email" name="email" placeholder="Email" required="">
									</p>
									
								</div>
								
								<div class="row">
									<p class="col-md-6 col-sm-6">
										<input class="form-control" type="password" name="password" placeholder="Password" required="">
									</p>
									<p class="col-md-6 col-sm-6">
										<input class="form-control" type="password" name="password_confirmation" placeholder="Confirmed Password" required="">
									</p>
									
									<p class="col-md-6 col-sm-6">
										<input class="form-control" type="date" name="datee" placeholder="Appoinment Date" required="" onKeyPress="return isNumberKey(event)" maxlength="11">
									</p>
									
								</div>

								 <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
             <div class="captcha">
               <span><?php echo captcha_img(); ?></span>
               <button type="button" class="btn btn-success"><i class="fa fa-refresh" id="refresh"></i></button>
               </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
             <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha"></div>
          </div>
								
								<div>
									<button id="reply_form_submit" type="submit" class="reply_submit_btn trans_300" value="Submit">
										Book Appointment
									</button>
								</div>

							</div>
                        </div>
                     
                    </div>
                
                </div>
<script type="text/javascript">
$('#refresh').click(function(){
  $.ajax({
     type:'GET',
     url:'refreshcaptcha',
     success:function(data){
        $(".captcha span").html(data.captcha);
     }
  });
});
</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.front.apphome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>