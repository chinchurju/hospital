<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.front.home-slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <section class="space-md">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <p class="lead hr-after">Our Services</p>
                            <p>Phasellus enim libero,ut et lobortis aliquam aliquam in tortor et libero, blandit vel sapi condimentum ultricies magn</p>
                        </div>
                        <div class="col-sm-9">
                            <div class="row margin-bottom-35">
                                <div class="col-sm-6">
                                    <div class="med-iconBox med-iconBox--left">
                                        <div class="med-iconBox-icon icon-big color-blue">
                                            <span class="icon-i-alternative-complementary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Web Design"></span>
                                        </div>
                                        <div class="med-iconBox-content">
                                            <h4 class="med-iconBox-title hr-after">
                                       Genetic Disorders
                                    </h4>
                                            <p>
                                                Praesent faucibus nisl sit amet nulla sollicitudin pretium a sed purus. Nullam bibendum porta magna.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.med box -->
                                <div class="col-sm-6">
                                    <div class="med-iconBox med-iconBox--left">
                                        <div class="med-iconBox-icon icon-big color-blue">
                                            <span class="icon-i-laboratory" data-toggle="tooltip" data-placement="top" title="" data-original-title="Web Design"></span>
                                        </div>
                                        <div class="med-iconBox-content">
                                            <h4 class="med-iconBox-title hr-after">
                                       Laboratory tests
                                    </h4>
                                            <p>
                                                Praesent faucibus nisl sit amet nulla sollicitudin pretium a sed purus. Nullam bibendum porta magna.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.med box -->
                            </div>
                            <div class="row margin-bottom-35">
                                <div class="col-sm-6">
                                    <div class="med-iconBox med-iconBox--left">
                                        <div class="med-iconBox-icon icon-big color-blue">
                                            <span class="icon-i-nutrition"></span>
                                        </div>
                                        <div class="med-iconBox-content">
                                            <h4 class="med-iconBox-title hr-after">
                                       Nutrition disorders
                                    </h4>
                                            <p>
                                                Praesent faucibus nisl sit amet nulla sollicitudin pretium a sed purus. Nullam bibendum porta magna.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.med box -->
                                <div class="col-sm-6">
                                    <div class="med-iconBox med-iconBox--left">
                                        <div class="med-iconBox-icon icon-big color-blue">
                                            <span class="icon-i-genetics" data-toggle="tooltip" data-placement="top" title="" data-original-title="Web Design"></span>
                                        </div>
                                        <div class="med-iconBox-content">
                                            <h4 class="med-iconBox-title hr-after">
                                       Genetics Disorders
                                    </h4>
                                            <p>
                                                Praesent faucibus nisl sit amet nulla sollicitudin pretium a sed purus. Nullam bibendum porta magna.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.med box -->
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="med-iconBox med-iconBox--left">
                                        <div class="med-iconBox-icon icon-big color-blue">
                                            <span class="icon-i-respiratory" data-toggle="tooltip" data-placement="top" title="" data-original-title="Web Design"></span>
                                        </div>
                                        <div class="med-iconBox-content">
                                            <h4 class="med-iconBox-title hr-after">
                                       Consultation
                                    </h4>
                                            <p>
                                                Praesent faucibus nisl sit amet nulla sollicitudin pretium a sed purus. Nullam bibendum porta magna.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.med box -->
                                <div class="col-sm-6">
                                    <div class="med-iconBox med-iconBox--left">
                                        <div class="med-iconBox-icon icon-big color-blue">
                                            <span class="icon-i-intensive-care" data-toggle="tooltip" data-placement="top" title="" data-original-title="Web Design"></span>
                                        </div>
                                        <div class="med-iconBox-content">
                                            <h4 class="med-iconBox-title hr-after">
                                       Intesive care
                                    </h4>
                                            <p>
                                                Praesent faucibus nisl sit amet nulla sollicitudin pretium a sed purus. Nullam bibendum porta magna.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.med box -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>

			<div class="benefits-quote bg-light">
                <div class="container-fluid tile-container">
                    <div class="row">
                        
                        <div class="col-sm-12 col-md-6 bg-image tile-item" >
						<image src="<?php echo asset('resources/assets/front'); ?>/images/image1.jpg" style="width: 95%;">
                        </div><div class="col-sm-12 col-md-6">
                            <div class="quote">
                                <p><a href="<?php echo e(route('login')); ?>">Login </a></p>
                                <blockquote><a href="<?php echo e(route('registration')); ?>">Register for an Appointment</a></blockquote>
                                
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.apphome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>