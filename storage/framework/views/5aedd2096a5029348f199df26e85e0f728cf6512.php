<?php $__env->startSection('content'); ?>
<div class="home">
	<div class="home_background_container prlx_parent">
		<div class="home_background prlx" style="background-image:url(<?php echo asset('resources/assets/front'); ?>/images/portfolio_background.jpg)"></div>
	</div>
	<div class="home_title">
		<h2>Services</h2>
		<div class="next_section_scroll">
			<div class="next_section nav_links" data-scroll-to=".portfolio">
				<i class="fas fa-chevron-down trans_200"></i>
				<i class="fas fa-chevron-down trans_200"></i>
			</div>
		</div>
	</div>
</div>
<section class="det">
	<div class="container">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4">
					<div class="det-lft">
						<ul>
						 <?php if(!$service->isEmpty()): ?>
						 <?php $__currentLoopData = $service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $services): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<li>
								<a href="<?php echo e(route('servicesdetails',$services->slug)); ?>" <?php if($services->slug==$servicesdetails->slug): ?> class="active" <?php endif; ?> ><?php echo $services->name; ?></a>
							</li>
						 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						 <?php endif; ?>	
						</ul>
					 </div>	
				</div>
				<div class="col-md-8 det-sec">
					<h3><?php echo $servicesdetails->name; ?></h3>
					<p><?php echo $servicesdetails->shortdes; ?></p>
 					 <?php if($servicesdetails->image): ?>
					<img src="<?php echo asset($servicesdetails->image); ?>" class="img-fluid">
				     <?php else: ?>
				     <?php endif; ?>
					 <?php echo $servicesdetails->description; ?>

					 <?php if(!$brands->isEmpty()): ?>
					<div class="brands">
					    <div class="row"><div class="col-md-12"><h3>Brands</h3></div>
						  	<div class="col-md-12">
						  		<div class="row">
								 <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							  		<div class="col-md-4"><a href="<?php echo $brand->url; ?>">
							  			<figure>
									 	 <?php if($brand->image): ?>
							  				<img src="<?php echo asset($brand->image); ?>">
										 <?php else: ?>
									     <?php endif; ?>
							  			</figure></a>
							  		</div>
							  	 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						  		</div>
						  	</div>
						</div>
					</div>
					 <?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.appservicesdet', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>