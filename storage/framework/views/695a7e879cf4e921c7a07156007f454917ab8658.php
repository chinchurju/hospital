<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo e(asset('resources/views/admin/images/admin_profile/1499174950.avatar5.png')); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo e($user->name); ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
			<li class="header">HOME</li>
			<li class="treeview <?php if(request()->segment(2) == 'listingCoupons' || request()->segment(2) == 'addCoupons'|| request()->segment(2) == 'editCoupons'): ?> active <?php endif; ?>">
				<a href="<?php echo e(route('admin.dashboard', ['reportBase' => 'this_month'])); ?>"> <i class="fa fa-home"></i><span> Dashboard</span></a>
			</li>
            <li class=" <?php if(request()->segment(2) == 'listingBanners'|| request()->segment(2) == 'editBanner' || request()->segment(2) == 'addBanner'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingBanners')); ?>">
                    <i class="fa  fa-picture-o"></i> <span>Banners</span>
                </a>
            </li>
            <li class=" <?php if(request()->segment(2) == 'listingAbout'|| request()->segment(2) == 'editAbout' || request()->segment(2) == 'addAbout'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingAbout')); ?>">
                    <i class="fa fa-user"></i> <span>About Us</span>
                </a>
            </li>
            <li class=" <?php if(request()->segment(2) == 'listingSolution'|| request()->segment(2) == 'editSolution' || request()->segment(2) == 'addSolution'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingSolution')); ?>">
                    <i class="fa fa-user"></i> <span>Solution</span>
                </a>
            </li>
            <li class=" <?php if(request()->segment(2) == 'listingService'|| request()->segment(2) == 'editService' || request()->segment(2) == 'addService'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingService')); ?>">
                    <i class="fa fa-user"></i> <span>Service</span>
                </a>
            </li>
            <li class=" <?php if(request()->segment(2) == 'listingBrands'|| request()->segment(2) == 'editBrands' || request()->segment(2) == 'addBrands'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingBrands')); ?>">
                    <i class="fa fa-user"></i> <span>Brand</span>
                </a>
            </li>
        </ul>
    </section>
</aside>

