<?php if(!$banners->isEmpty()): ?>
<div class="home">
	<div class="hero_slider_container">
		<div class="owl-carousel owl-theme hero_slider">
 		  <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div class="owl-item hero_slider_item item_1 d-flex flex-column align-items-center justify-content-center">
				<span><?php echo $banner->banners_title; ?></span>
				<span><?php echo $banner->banners_desc; ?></span>
				<span><?php echo $banner->banners_link_text; ?></span>
				<img src="<?php echo e(asset("$banner->banners_image")); ?>">
			</div>
		  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
		<div class="hero_slider_nav hero_slider_nav_left">
			<div class="hero_slider_prev d-flex flex-column align-items-center justify-content-center trans_200">
				<i class="fas fa-chevron-left trans_200"></i>
			</div>
		</div>
		<div class="hero_slider_nav hero_slider_nav_right">
			<div class="hero_slider_next d-flex flex-column align-items-center justify-content-center trans_200">
				<i class="fas fa-chevron-right trans_200"></i>
			</div>
		</div>
	</div>
</div>
<?php endif; ?> 