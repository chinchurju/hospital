<?php $__env->startSection('content'); ?>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ModernClinic</title>
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">
        <link href="<?php echo asset('resources/assets/front'); ?>/assets/css/lib/weather-icons.css" rel="stylesheet" />
        <link href="<?php echo asset('resources/assets/front'); ?>/assets/css/lib/owl.carousel.min.css" rel="stylesheet" />
        <link href="<?php echo asset('resources/assets/front'); ?>/assets/css/lib/owl.theme.default.min.css" rel="stylesheet" />
        <link href="<?php echo asset('resources/assets/front'); ?>/assets/css/lib/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo asset('resources/assets/front'); ?>/assets/css/lib/themify-icons.css" rel="stylesheet">
        <link href="<?php echo asset('resources/assets/front'); ?>/assets/css/lib/menubar/sidebar.css" rel="stylesheet">
        <link href="<?php echo asset('resources/assets/front'); ?>/assets/css/lib/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo asset('resources/assets/front'); ?>/assets/css/lib/helper.css" rel="stylesheet">
        <link href="<?php echo asset('resources/assets/front'); ?>/assets/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <div class="logo"><a href="index.html"><!-- <img src="assets/images/logo.png" alt="" /> --><span>ModernClinic</span></a></div>
                    <ul>
					<li><a href="<?php echo e(route('appointment_new')); ?>"><i class="ti-calendar"></i> New Appointment </a></li>
						<li><a href="<?php echo e(route('appointment')); ?>"><i class="ti-calendar"></i> Appointments </a></li>
                      <li><a href="<?php echo e(route('medicine')); ?>"><i class="ti-calendar"></i> Medicine Details </a></li>
                        <li><a href="<?php echo e(route('deceased')); ?>"><i class="ti-email"></i> Disease Description</a></li>
                       <li><a href="<?php echo e(route('app-profile')); ?>"><i class="ti-user"></i> Profile</a></li>
                        <li><a href="<?php echo e(route('logout')); ?>"><i class="ti-calendar"></i> logout </a></li>
                       
                    </ul>
                </div>
            </div>
        </div>
        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    <section id="main-content">
                       
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title"><b><u>Details</u></b></h4>
                                         <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                                             Name : <p><?php echo e(auth()->user()->name); ?></p>
                                             Medicine : <p><?php echo $user->medicine; ?></p>
                                             Medicine Description : <p><?php echo $user->medicinedesc; ?></p>
                                             Time : <p><?php echo $user->time; ?></p>
                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                         <a href="<?php echo e(route('generatemedicinePDF')); ?>" type="button" class="btn btn-default">PDF</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div id="search">
            <button type="button" class="close">×</button>
            <form>
                <input type="search" value="" placeholder="type keyword(s) here" />
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/jquery.min.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/jquery.nanoscroller.min.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/menubar/sidebar.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/preloader/pace.min.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/bootstrap.min.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/circle-progress/circle-progress.min.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/circle-progress/circle-progress-init.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/morris-chart/raphael-min.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/morris-chart/morris.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/morris-chart/morris-init.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/flot-chart/jquery.flot.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/flot-chart/jquery.flot.resize.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/flot-chart/flot-chart-init.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/jquery.vmap.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/jquery.vmap.min.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/jquery.vmap.sampledata.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.world.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.algeria.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.argentina.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.brazil.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.france.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.germany.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.greece.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.iran.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.iraq.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.russia.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.tunisia.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.europe.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/country/jquery.vmap.usa.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/vector-map/vector.init.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/weather/jquery.simpleWeather.min.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/weather/weather-init.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/lib/owl-carousel/owl.carousel-init.js"></script>
        <script src="<?php echo asset('resources/assets/front'); ?>/assets/js/scripts.js"></script>
    </body>
</html>


<!-- <div class="container space-sm">
    <div class="col-md-12">
        <div class="book-box row">
            <div class="book-form">
                <h4 class="hr-after" style="text-align: center;">Details</h4>
                <div class="row">
                    <?php echo e(auth()->user()->name); ?>


                </div>
			</div>
        </div>
    </div>
</div> -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.appcus', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>