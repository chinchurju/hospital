<?php $__env->startSection('content'); ?>

<div class="home">
	<div class="home_background_container prlx_parent">
		<div class="home_background prlx" style="background-image:url(<?php echo asset('resources/assets/front'); ?>/images/home_background.jpg)"></div>
	</div>
	<div class="home_title">
		<h2>Solution</h2>
		<div class="next_section_scroll">
			<div class="next_section nav_links" data-scroll-to=".service_boxes">
				<i class="fas fa-chevron-down trans_200"></i>
				<i class="fas fa-chevron-down trans_200"></i>
			</div>
		</div>
	</div>
</div>



<section class="service">
	<div class="container">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-6">
					<p>ALSAQR is a Security Solutions Provider company always keeping its services up-to-date with the latest trends on the market, providing its customers with high-end-class and easily extensible Security System products. 
Solutions we offer range from Low to High end Security Solutions of any complexity. 
</p>
				</div>
				<div class="col-md-6">
					<p>The company's staff will help you to efficiently secure your business, develop a solution that would fully correspond to all your business needs and/or improve the existing system. Our guidelines combined with utmost accuracy while working with details have proved to be a perfect union, which enables our highly proficient personnel provide our clients with technically efficient, reliable and affordable solutions</p>
				</div>
			</div>
			<div class="ser-sec">
				<?php if(!$solution->isEmpty()): ?>
					<?php $__currentLoopData = $solution; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$solution): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="row <?php if($key%2==1): ?> flex-row-reverse <?php endif; ?>">
					<div class="col-md-6 p-0"><img src="<?php echo $solution->image; ?>"></div>
					<div class="col-md-6 p-0">
						<div class="ser-txt">
							<h4><?php echo $solution->name; ?></h4>
							<p><?php echo $solution->description; ?></p>
						</div>
					</div>
				</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
				<!--<div class="row flex-row-reverse">
					<div class="col-md-6 p-0"><img src="<?php echo asset('resources/assets/front'); ?>/images/school-education.jpg"></div>
					<div class="col-md-6 p-0">
						<div class="ser-txt">
							<h4>Schools & Education </h4>
							<p>CCTV, access Control, Telefleet- School bus solution ,networking Biometric attendance system</p>
						</div>
					</div>
				</div>-->
				
			</div>
		</div>
	</div>
</section>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.appsolution', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>