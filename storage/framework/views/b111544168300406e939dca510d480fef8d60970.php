<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.front.home-slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<section class="ceo">
	<div class="container">
		<div class="row">
			<div class="col-md-5" data-aos="fade-up"><div class="ceo-img"><img src="<?php echo asset('resources/assets/front'); ?>/images/khalid_alrabban.png"></div></div>
			<div class="col-md-7 ceo-sec"  data-aos="fade-up">
				<h4>Since its establishment in 1964, Rabban Group has kept pace with the rapid developments in the State of Qatar to become a major contractor and to build an enviable reputation for quality and delivering projects on time and within budget. </h4>
				<h5>Khalid Mohamed Al Rabban<br> <span>Founding Chairman – Al Rabban Holding.</span> </h5>
			</div>
		</div>
	</div>
</section>

<section class="bd clearfix">
	<div class="container">
		<h4>BOARD OF DIRECTORS <br> <span>AL RABBAN HOLDING</span> <div class="scroll-down"><div class="scroll-down__line"></div></div></h4>
		<ul class="clearfix">
			<?php if(!$board->isEmpty()): ?>
		   	<?php $__currentLoopData = $board; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $boardof): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<li data-aos="fade-up"><a href="<?php echo e(route('board')); ?>">
				<img src="<?php echo asset($boardof->image); ?>">
				<h5><?php echo e($boardof->name); ?><br> <span> <?php echo e($boardof->position); ?></span> </h5></a>
			</li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
		</ul>
	</div>
</section>

<section class="sec1">
	<div class="container">
		<div class="col-md-12">
			<div class="sec1-sec">
			<img src="<?php echo asset('resources/assets/front'); ?>/images/sec1.png" class="module">
				<h4  data-aos="fade-up">Over the years Qatar has experienced an enormous growth, and is currently moving into an accelerated period of growth, laying the foundation for the achievement of the Qatar National Vision 2030.</h4>
				<h5 data-aos="fade-up">KHALID AL RABBAN <br> <span>Founding Chairman – Al Rabban Holding.</span> </h5>
			</div>
		</div>
	</div>
</section>

<section class="osub clearfix">
	<div class="container clearfix">
		<div class="col-md-12 clearfix">
			<h3>OUR SUBSIDIARIES </h3>
			<h5>Tremendous growth within a diversified portfolio of Companies and Partnerships has resulted in the creation of the Al Rabban Holding.  <div class="scroll-down"><div class="scroll-down__line"></div></div></h5>
			<?php if(!$company->isEmpty()): ?>
		   	<?php $__currentLoopData = array_chunk($company->toArray(), 2); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $companies): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<ul class="clearfix">
				 <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $companys): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
				<li data-aos="fade-up"><a href="<?php echo e(route('comdetails',$companys->slug)); ?>">
					<div class="osub-txt"><img src="<?php echo asset($companys->logo); ?>">
					<h4><?php echo $companys->company_name; ?></h4>
					<p><?php echo e($companys->short_desc); ?></p>
					</div>
					<div class="osub-img"><div class="scroll-down"><div class="scroll-down__line"></div></div><img src="<?php echo asset($companys->small_image); ?>"></div>
					</a>
				</li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</ul>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
		</div>
	</div>
</section>

<section class="ne">
<h3>NEWS & EVENTS <br> <span>AL RABBAN HOLDING</span></h3>
	<div class="os-sec">
		<div class="owl-carousel" id="owl-demo2">
			<?php if(!$news->isEmpty()): ?>
		   	<?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div><img src="<?php echo asset($newss->image); ?>">
				<h5><?php echo e($newss->name); ?></h5>
				<p><?php echo e($newss->short_desc); ?></p>
				<a href="news-events.html">KNOW MORE</a>
			</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
		</div>
	</div>	
</section>

 <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.apphome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>