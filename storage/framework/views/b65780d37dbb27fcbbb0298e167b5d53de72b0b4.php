<?php $__env->startSection('content'); ?>

	<div class="home">
		<div class="home_background_container prlx_parent">
			<div class="home_background prlx" style="background-image:url(<?php echo asset('resources/assets/front'); ?>/images/portfolio_background.jpg)"></div>
		</div>
		
		<div class="home_title">
			<h2>Services</h2>
			<div class="next_section_scroll">
				<div class="next_section nav_links" data-scroll-to=".portfolio">
					<i class="fas fa-chevron-down trans_200"></i>
					<i class="fas fa-chevron-down trans_200"></i>
				</div>
			</div>
		</div>
	
	</div>

	<!-- Portfolio -->

	<div class="portfolio">
		
		<div class="container">
			<div class="row">
				<?php if(!$service->isEmpty()): ?>
					<?php $__currentLoopData = $service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $services): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="card branding">
							<div class="card_image">
								<img class="card-img-top" src="<?php echo $services->image; ?>" >
							</div>
							<div class="card-body">
								<div class="card-header">Service</div>
								<div class="card-title"><?php echo $services->name; ?></div>
								<div class="card-text"><?php echo $services->shortdes; ?> </div>
								<div class="card-link"><a href="<?php echo e(route('servicesdetails',$services->slug)); ?>">read more</a></div>
							</div>
						</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>	

			</div>

			
		</div>
	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.appservices', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>