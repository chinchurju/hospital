<?php $__env->startSection('content'); ?>
<div class="main">


<div class="breadcrumb lst-bread"> <a class="breadcrumb-item" href="<?php echo e(route('home')); ?>">Home</a> <span class="breadcrumb-item active">Login</span> </div>

	<div class="login">
		<div class="row">
	
			<div class="col-md-4">
			</div>
			
			<div class="col-md-4">
				<h4>Customers Login <br> <span>If you have an account with us, please log in.</span></h4>
				 <div class="col-md-12"><?php echo $__env->make('layouts.errors-and-messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></div>
				 <form action="<?php echo e(route('login')); ?>" method="post" >
					<?php echo e(csrf_field()); ?>

					<div class="form-group clearfix"><i class="fa fa-envelope-o"></i>
					<!--<input type="text" class="form-control" placeholder="Email Address">-->
					<input type="name" class="form-control" placeholder="Email" required id="email" name="email" value="<?php echo e(old('email')); ?>" style="border-radius:5px 5px 0 0">

					</div>
					<div class="form-group clearfix"><i class="fa fa-key"></i>
					<!--<input type="password" class="form-control" placeholder="Password">-->
					<input type="password" name="password" id="password" value="" class="form-control" placeholder="Password" required style="border-radius:0 0 5px 5px">

					</div>

					
								 <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
             <div class="captcha">
               <span><?php echo captcha_img(); ?></span>
               <button type="button" class="btn btn-success"><i class="fa fa-refresh" id="refresh"></i></button>
               </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
             <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha"></div>
          </div>
				<!-- 	<div class="form-group clearfix"><a href="<?php echo e(route('password.request')); ?>">Forgot Your Password?</a></div> -->
					<div class="form-group clearfix"><button class="btn-log">Login</button></div>
					<!-- <p>Don't Have an Account? <a href="<?php echo e(route('register')); ?>"> Sign up </a></p> -->
				</form>
			</div>
					<div class="col-md-4">
					</div>
					</div></div>
		</div>
	</div>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.apphome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>