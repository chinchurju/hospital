<?php $__env->startSection('content'); ?>
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Edit Details</h2>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
              		<div class="box box-info">
                    <br>                       
                        <?php if(count($errors) > 0): ?>
                              <?php if($errors->any()): ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <?php echo e($errors->first()); ?>

                                </div>
                              <?php endif; ?>
                          <?php endif; ?>
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit category</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->  
						<div class="panel with-nav-tabs  panel-default">
							<!--<div class="panel-heading" style="border:none;">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#English" data-toggle="tab">English</a></li>
										<li><a href="#Arabic" data-toggle="tab">Arabic</a></li>
									</ul>
							</div>-->
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="English">
						
										 <div class="box-body">
										 
											<?php echo Form::open(array('url' =>'admin/updateService', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>

											  
						<?php echo Form::hidden('id',  $result['service'][0]->id , array('class'=>'form-control', 'id'=>'id')); ?>


						<?php echo Form::hidden('oldImage',  $result['service'][0]->image, array('id'=>'oldImage')); ?>

						<?php echo Form::hidden('oldSmallImage',  $result['service'][0]->shortimage, array('id'=>'oldSmallImage')); ?>


												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Title')); ?> </label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('name', $result['service'][0]->name, array('class'=>'form-control','id'=>'name')); ?>

												  </div>
												</div>

												<div class="form-group">
					                                <label for="name" class="col-sm-2 col-md-3 control-label">Short Description </label>
					                                <div class="col-sm-10 col-md-8">
					                                  <textarea id="editor" name="shortdes" class="form-control" rows="10" cols="80"><?php echo $result['service'][0]->shortdes; ?></textarea> <br>
					                                </div>
				                             	</div>
												
												<div class="form-group">
					                                <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Description')); ?>  </label>
					                                <div class="col-sm-10 col-md-8">
					                                  <textarea id="editor" name="description" class="form-control" rows="10" cols="80"><?php echo $result['service'][0]->description; ?></textarea> <br>
					                                </div>
				                             	</div>

											  
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Image')); ?></label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::file('newImage', array('id'=>'service')); ?>

													<br>
												<img src="<?php echo e(asset('').$result['service'][0]->image); ?>" alt="" width=" 100px">
												  </div>
												</div>

												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Small Image</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::file('shortimage', array('id'=>'service')); ?>

													<br>
													<img src="<?php echo e(asset('').$result['service'][0]->shortimage); ?>" alt="" width=" 100px">
												  </div>
												</div>


												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Status')); ?></label>
												  <div class="col-sm-10 col-md-4">
													  <select class="form-control" name="status">
														  <option value="Active" <?php if($result['service'][0]->status=='Active'): ?> selected <?php endif; ?>><?php echo e(trans('labels.Active')); ?></option>
														  <option value="Inactive" <?php if($result['service'][0]->status=='Inactive'): ?> selected <?php endif; ?>><?php echo e(trans('labels.Inactive')); ?></option>
													  </select>
												  </div>
												</div>
												
											  <!-- /.box-body -->
											  <div class="box-footer text-center">
												<button type="submit" class="btn btn-primary"><?php echo e(trans('labels.Update')); ?></button>
												<a href="<?php echo e(URL::to('admin/listingService')); ?>" type="button" class="btn btn-default"><?php echo e(trans('labels.back')); ?></a>
											  </div>
											  <!-- /.box-footer -->
											<?php echo Form::close(); ?>

										</div>
									</div>
							
						
                  </div>
              </div>
            </div>
            
          </div>
         <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
   
  </section>
  <!-- /.content --> 

   <script src="<?php echo asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>


<script type="text/javascript">
		$(function () {
			
			//bootstrap WYSIHTML5 - text editor
			//
			$("textarea").summernote({height: "400px",});
			//$("textarea").wysihtml5();
			
    });
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>