<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name')); ?></title>
    <link rel="stylesheet" href="<?php echo e(asset('css/admin.min.css')); ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    
	<link href="<?php echo asset('resources/views/admin/bootstrap/css/bootstrap.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  <link href="<?php echo asset('resources/views/admin/bootstrap/css/styles.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  <!-- Font Awesome -->
  <link href="<?php echo asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo asset('resources/views/admin/plugins/select2/select2.min.css'); ?>">
  
    <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo asset('resources/views/admin/plugins/colorpicker/bootstrap-colorpicker.min.css'); ?>">
  <!-- Ionicons -->
  <link href="<?php echo asset('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo asset('resources/views/admin/plugins/daterangepicker/daterangepicker-bs3.css'); ?>">
   <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo asset('resources/views/admin/plugins/datepicker/datepicker3.css'); ?>">
  <!-- jvectormap -->
  <link href="<?php echo asset('resources/views/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link href="<?php echo asset('resources/views/admin/dist/css/AdminLTE.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link href="<?php echo asset('resources/views/admin/dist/css/skins/_all-skins.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="<?php echo asset('resources/views/admin/plugins/iCheck/all.css'); ?>" media="all" rel="stylesheet" type="text/css" />
	
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo e(asset('resources/views/images/favicon.ico')); ?>">
</head>
<body class="hold-transition skin-purple login-page">
    <div class="login-box">
        <div class="login-logo">
			<img src="<?php echo e(asset('resources/assets/front/images/logo-dark.png')); ?>" class="ionic-hide" style="max-width: 52%;"> 
			<img src="<?php echo e(asset('resources/assets/front/images/logo-dark.png')); ?>" class="android-hide"> 
			<div style="
			font-size: 25px;
		"><b> Welcome</b>To Admin Panel</div>
		  </div>
		
        <!-- /.login-logo -->
        <?php echo $__env->make('layouts.errors-and-messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <form action="<?php echo e(route('admin.login')); ?>" method="post">
                <?php echo e(csrf_field()); ?>

                <div class="form-group has-feedback">
                    <input name="email" type="email" class="form-control" placeholder="Email" value="<?php echo e(old('email')); ?>">
                    <span class="fa fa-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input name="password" type="password" class="form-control" placeholder="Password">
                    <span class="fa fa-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">

                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    
    <script src="<?php echo e(asset('js/admin.min.js')); ?>"></script>
</body>
</html>