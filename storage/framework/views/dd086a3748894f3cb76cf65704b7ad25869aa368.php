<?php $__env->startSection('content'); ?>

<section class="tri">
	
	<div class="container clearfix">
		<div class="col-md-12">
			<ul class="clearfix">
				<li><div class="tr-txt"><h4>Khalid M Al Rabban</h4><h5>1940 </h5><h5>2014 </h5></div></li>
				<li><div class="tr-img"><img src="images/ceo.png"></div></li>
			</ul>
		</div>
	</div>
	
</section>

<section class="bio">
	
	<div class="container">
		<div class="col-md-12">
			<h4 data-aos="fade-up">Changing with the times</h4>
			<h5 data-aos="fade-up">Biography</h5>
			<p data-aos="fade-up" >Born shortly after oil was first discovered near Dukhan in 1940, Khalid Rabban enjoyed his younger years with much outdoor activity including sailing and diving off the small port of Doha, or kicking a ball around on the beach. Wind towers on the small limestone buildings provided relief from the summer heat, and Qatar enjoyed a very quiet and peaceful existence. The world war delayed the exploitation of oil and gas until about 10 years later.
Little could anyone realise at the time how dramatically things would change.
Whist today Qatar still enjoys peace, it can hardly be called quiet. Change has been dramatic and development quite incredible over the last half a century.

</p>
			<div class="scroll-down"><div class="scroll-down__line"></div></div>
		</div>
	</div>
	
</section>

<section class="achieve tr-achi">
	
	<div class="container">
		<div class="col-md-12">
		  <h3 data-aos="fade-up">Achievements <div class="scroll-down"><div class="scroll-down__line"></div></div></h3>
			<ul data-aos="fade-up">
				<li class="active"><h4>19<span>62</span> <img src="images/khalid_alrabban-bkp.png"></h4><p>Khalid Rabban’s father prepared him well for the role he was to play in the development of his country. Having completed school in Qatar, he was sent off to England in 1962 for three years to master English and experience life in a very different and industrialised society. </p></li>
				<li><h4>19<span>70</span> <img src="images/khalid_alrabban-bkp.png"></h4><p>Thereafter he spent time in Lebanon at the Arab University in Beirut studying economics and political science before returning to Qatar in 1970 to join his father in business.</p></li>
				<li><h4>19<span>85</span> <img src="images/khalid_alrabban-bkp.png"></h4><p>In 1985, the expanding Group started bottling mineral water and today Rayyan bottled water is the most widely consumed, competing against several international brands and certainly one on the most recognized purely Qatari brands. Again, its success has been predicated on the highest quality standards.</p></li>
				<li><h4>20<span>06</span> <img src="images/khalid_alrabban-bkp.png"></h4><p>By 2006, Qatar’s growth was threatened by a scarcity of materials for concrete.
In response to this need, and at the request of the Emir, Khalid Rabban was asked to come up with a solution. This lead to the creation of Qatar Primary Materials Company, a vast organisation stretching from quarries in other countries to new sand plants in Qatar and involving newly acquired ships and even construction of harbourage and materials handling facilities. 
</p></li>
				
			</ul>
		</div>
	</div>
	
</section>

<section class="sec1">
	
	<div class="container">
		<div class="col-md-12">
			<div class="sec1-sec">
			<img src="images/sec1.png" data-aos="fade-up">
				<h4 data-aos="fade-up">Its Chairman and founder Khalid Rabban continues to work hard in changing times. Whilst the fishing trips are fewer these days, Khalid Rabban plays an active role in Qatar’s Natural History Society and also in other matters involving Qatar’s expanding future, truly binding in the best of the past with the best of what is still to come.</h4>
			</div>
		</div>
	</div>
	
</section>

<section class="sec2">
	<div class="container">
		<div class="col-md-12">
		<h3>Gallery</h3>
			<div class="sec2-img" data-aos="fade-up"><img src="images/sec2.png" class="img-fluid"></div>
		</div>
	</div>
</section>
		            
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>