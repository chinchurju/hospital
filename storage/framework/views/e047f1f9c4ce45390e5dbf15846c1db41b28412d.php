<?php $__env->startSection('content'); ?>
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Edit Details</h2>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
              		<div class="box box-info">
                    <br>                       
                        <?php if(count($errors) > 0): ?>
                              <?php if($errors->any()): ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <?php echo e($errors->first()); ?>

                                </div>
                              <?php endif; ?>
                          <?php endif; ?>
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit category</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->  
						<div class="panel with-nav-tabs  panel-default">
							<!--<div class="panel-heading" style="border:none;">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#English" data-toggle="tab">English</a></li>
										<li><a href="#Arabic" data-toggle="tab">Arabic</a></li>
									</ul>
							</div>-->
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="English">
									
									
									
						
										 <div class="box-body">
										 
											<?php echo Form::open(array('url' =>'admin/updateAppointment', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>

											  
						<?php echo Form::hidden('id',  $result['appointments'][0]->id , array('class'=>'form-control', 'id'=>'id')); ?>


						
						<h3>Personal Details</h3>
									
									
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Name</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('name', $result['appointments'][0]->name, array('class'=>'form-control','id'=>'name','readonly')); ?>

												  </div>
												</div>
												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Address</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('address', $result['appointments'][0]->address, array('class'=>'form-control','id'=>'address','readonly')); ?>

												  </div>
												</div>
												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Date of Birth</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('dob', $result['appointments'][0]->dob, array('class'=>'form-control','id'=>'dob','readonly')); ?>

												  </div>
												</div>
												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Gender</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('gender', $result['appointments'][0]->gender, array('class'=>'form-control','id'=>'gender','readonly')); ?>

												  </div>
												</div>
												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Place</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('place', $result['appointments'][0]->place, array('class'=>'form-control','id'=>'place','readonly')); ?>

												  </div>
												</div>
												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Phone Number</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('phone', $result['appointments'][0]->phone, array('class'=>'form-control','id'=>'phone','readonly')); ?>

												  </div>
												</div>

												
													<h3>Appointment Details</h3>

												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Register Id</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('regid', $result['appointments'][0]->regid, array('class'=>'form-control','id'=>'regid','readonly')); ?>

												  </div>
												</div>

												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Date</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('datee', $result['appointments'][0]->datee, array('class'=>'form-control','id'=>'datee','readonly')); ?>

												  </div>
												</div>

												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Token Number</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('tokenno', $result['appointments'][0]->tokenno, array('class'=>'form-control','id'=>'tokenno','readonly')); ?>

												  </div>
												</div>
												
												

											  												
												<div class="form-group">
												  	<label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Status')); ?></label>
												  	<div class="col-sm-10 col-md-4">
													  	<select class="form-control" name="status">
														  	<option value="Active" <?php if($result['appointments'][0]->status=='Active'): ?> selected <?php endif; ?>><?php echo e(trans('labels.Active')); ?></option>
														  	<option value="Consulting Completed" <?php if($result['appointments'][0]->status=='Consulting Completed'): ?> selected <?php endif; ?>>Consulting Completed</option>
														<!--   	<option value="Done" <?php if($result['appointments'][0]->status=='Done'): ?> selected <?php endif; ?>>Done</option> -->
													  	</select>
												 	</div>
												</div>
												
											  <!-- /.box-body -->
											  <div class="box-footer text-center">
												<button type="submit" class="btn btn-primary"><?php echo e(trans('labels.Update')); ?></button>
												<a href="<?php echo e(URL::to('admin/listingAppointment')); ?>" type="button" class="btn btn-default"><?php echo e(trans('labels.back')); ?></a>
											  </div>
											  <!-- /.box-footer -->
											<?php echo Form::close(); ?>

										</div>
									</div>
							
						
                  </div>
              </div>
            </div>
            
          </div>
         <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
   
  </section>
  <!-- /.content --> 

   <script src="<?php echo asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>


<script type="text/javascript">
		$(function () {
			
			//bootstrap WYSIHTML5 - text editor
			//
			$("textarea").summernote({height: "400px",});
			//$("textarea").wysihtml5();
			
    });
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>