  <div class="footer-4cln space-md">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-3 col-xs-6">
                                    <h6>Company</h6>
                                    <ul class="list-unstyled">
                                        <li><a href="#">About</a>
                                        </li>
                                        <li><a href="#">Meet the team</a>
                                        </li>
                                        <li><a href="#">Blog</a>
                                        </li>
                                        <li><a href="#">Contact Us</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h6>Account</h6>
                                    <ul class="list-unstyled">
                                        <li><a href="#">Payments</a>
                                        </li>
                                        <li><a href="#">Subscriptions</a>
                                        </li>
                                        <li><a href="#">Gift Card</a>
                                        </li>
                                        <li><a href="#">Support</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h6>Help</h6>
                                    <ul class="list-unstyled">
                                        <li><a href="#">Payments</a>
                                        </li>
                                        <li><a href="#">Subscriptions</a>
                                        </li>
                                        <li><a href="#">Gift Card</a>
                                        </li>
                                        <li><a href="#">Support</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h6>Legal</h6>
                                    <ul class="list-unstyled">
                                        <li><a href="#">Payments</a>
                                        </li>
                                        <li><a href="#">Subscriptions</a>
                                        </li>
                                        <li><a href="#">Gift Card</a>
                                        </li>
                                        <li><a href="#">Settings</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h6>Subscribe to our newsletter</h6>
                            <form class="form-inline" role="form">
                                <div class="input-group">
                                    <input type="email" class="form-control footernews" placeholder="Enter Email">
                                    <span class="input-group-btn">
                           <button class="btn btn-info" type="button">Subscribe</button>
                           </span>
                                </div>
                                <span class="help-block">We'll never share your address.</span>
                            </form>
                            <div class="ourTeam-social">
                                <ul class="social">
                                    <li><a href="#" class="social-btn social-facebook" title="Facebook"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li><a href="#" class="social-btn social-twitter" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li><a href="#" class="social-btn social-google" title="Google+"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li><a href="#" class="social-btn social-linkedin" title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="footer-4cln-bar">
                        <span class="phone"><i class="fa fa-phone"></i> 1 (800) 888-8888</span>
                        <span class="email"> <a href="http://localhost/party/admin/login">Admin</a></span>
                        <span class="address">Copyright &copy; Website, Inc. 2014<br/> 1234 1st Ave, NY 10007</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: animatsion -->
    </div>