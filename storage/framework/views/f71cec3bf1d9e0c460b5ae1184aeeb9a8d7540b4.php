<?php $__env->startSection('content'); ?>
<div class="main">


<div class="breadcrumb lst-bread"> <a class="breadcrumb-item" href="<?php echo e(route('home')); ?>">Home</a> <span class="breadcrumb-item active">Registration</span> </div>

	<div class="login">
		<div class="row">
			<div class="col-md-6"><div class="lgn-lft"><img src="<?php echo asset('resources/assets/front/'); ?>/images/login-lft.jpg" class="img-fluid"></div></div>
			<div class="col-md-6">
				<h4>Create an Account </h4>
				 <form  role="form" method="POST" action="<?php echo e(route('register')); ?>">
                        <?php echo e(csrf_field()); ?>

                    
					
					 <div class="form-group clearfix">
						<input type="text" class="form-control" placeholder="First Name" required name="name" value="<?php echo e(old('name')); ?>" autofocus style="border-radius:5px 5px 0 0">
						<?php if($errors->has('name')): ?>
							<span class="help-block">
								<strong><?php echo e($errors->first('name')); ?></strong>
							</span>
                        <?php endif; ?>
                    </div>
                    
					<div class="form-group clearfix">
						<input type="text" class="form-control" placeholder="Last Name" required name="lname" value="<?php echo e(old('lname')); ?>">
						<?php if($errors->has('lname')): ?>
							<span class="help-block">
								<strong><?php echo e($errors->first('lname')); ?></strong>
							</span>
                        <?php endif; ?>
                    </div>
					<!--<div class="form-group clearfix"><i class="fa fa-envelope-o"></i><input type="text" class="form-control" placeholder="Email Address"></div>
					<div class="form-group clearfix"><i class="fa fa-key"></i><input type="password" class="form-control" placeholder="New Password"></div>
					<div class="form-group clearfix"><i class="fa fa-key"></i><input type="password" class="form-control" placeholder="Retype Password"></div>
					-->
					
					<div class="form-group clearfix">
					<input type="text" class="form-control" placeholder="Email Address" required name="email" value="<?php echo e(old('email')); ?>">
					<?php if($errors->has('email')): ?>
							<span class="help-block">
								<strong><?php echo e($errors->first('email')); ?></strong>
							</span>
                        <?php endif; ?>
					</div>
					<div class="form-group clearfix">
					<input type="password" class="form-control" placeholder="New Password" required  name="password">
					</div>
					<div class="form-group clearfix">
					<input type="password" class="form-control" placeholder="Retype Password" required name="password_confirmation">
					</div>
					
					<!-- <div class="form-group clearfix">
					<input type="text" class="form-control" placeholder="Your Email" required name="email" value="<?php echo e(old('email')); ?>">
						<?php if($errors->has('email')): ?>
							<span class="help-block">
								<strong><?php echo e($errors->first('email')); ?></strong>
							</span>
                        <?php endif; ?>
                    </div>
                    
                    <div class="form-group clearfix">
							<input type="password" class="form-control" placeholder="Password" required  name="password">
                    </div>
                    
                    <div class="form-group clearfix">
						<input type="password" class="form-control" placeholder="Confirm Password" required name="password_confirmation" style="border-radius: 0 0 5px 5px">
                    </div>
					
					-->
                    
                    <!--<button type="submit" class="log">Register</button>-->
					
					<div class="form-group clearfix"><button type="submit" class="btn-log">Create account</button></div>
					<p>Already you have account?  <a href="<?php echo e(route('login')); ?>"> Login </a></p>
				</form>
			</div>
		</div>
	</div>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>